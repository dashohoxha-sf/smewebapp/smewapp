#!/bin/bash
### Create (initialize) a new application based on the SMEWebApp framework. 

if [ "$1" = "" ]
then
  echo "Usage: $0 destination [vars]"
  echo "Create (initialize) a new app based on the SMEWebApp framework." 
  echo "'destination' is the directory where the application will be created."
  echo "The second optional parameter is a file that contains the variables."
  exit 0
fi

destination=$1

### check that $destination is not a subversion working copy
not_working_copy=$(svn info $destination 2>&1 | grep 'not a working copy')
if [ "$not_working_copy" = "" ]
then
  echo "$destination should not be a subversion working copy"
  exit 1
fi

### get the values of the application variables
if [ "$2" != "" ]
then
  . $2
else
  read -p "appname[testapp]=" appname
  appname=${appname:-testapp}
  read -p "svnroot[/data/svn/smewebapp]=" svnroot
  read -p "dbhost[localhost]=" dbhost
  read -p "dbname[$appname]=" dbname
  read -p "dbuser[root]=" dbuser
  read -p "dbpasswd[]=" dbpasswd
fi
appname=${appname:-testapp}
svnroot=${svnroot:-/data/svn/smewebapp}
dbhost=${dbhost:-localhost}
dbname=${dbname:-$appname}
dbuser=${dbuser:-root}

### get the svn url of the framework
smewapp_dir=$(dirname $(dirname $0))
smewapp_url=$(svn info $smewapp_dir | grep URL | cut -d' ' -f2)

### get the latest version of the framework
rev=$(svnlook youngest $svnroot/smewapp/)

### export a copy of the framework to the destination directory
svn export $smewapp_url $destination/$appname -r $rev

### save the revision of the framework that is exported
echo $rev > $destination/$appname/tools/smewapp_current_revision.txt

### save variables in a configuration file
cat <<EOF > $destination/$appname/initial-modules/config.sh
### This file contains some variables that are used
### by the scripts that handle modules (e.g. application
### name, database name, etc.) which are the same for
### all the modules.

appname=$appname
svnroot=$svnroot
dbhost=$dbhost
dbname=$dbname
dbuser=$dbuser
dbpasswd=$dbpasswd
EOF

### modify 'config/const.DB.php'
cat <<EOF > $destination/$appname/config/const.DB.php
<?php
define("DBHOST", "$dbhost");
define("DBUSER", "$dbuser");
define("DBPASS", "$dbpasswd");
define("DBNAME", "$dbname");
?>
EOF

### modify l10n scripts
find $destination/$appname/l10n/ -name '*.sh' \
   | xargs sed s/smewapp/$appname/g -i
rm -f $destination/$appname/l10n/compendia/{*.sh,README}

### create a svn repository for the new application
svnadmin create $svnroot/$appname

### import the application into the repository
msg="initial version created from the framework"
svn import $destination/$appname file://$svnroot/$appname/trunk -m "$msg"

### checkout from repository a working copy
rm -rf $destination/$appname
svn checkout file://$svnroot/$appname/trunk $destination/$appname

### create the database of the application
echo "create database $dbname" | mysql --user=$dbuser --password="$dbpasswd"
