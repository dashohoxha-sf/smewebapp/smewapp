#!/bin/bash
### get the latest changes of the smewapp framework
### and merge them to the application
### Note: this does the same thing as update_framework.sh, but is simpler

### check that this is not the framework itself, but an application
is_smewapp=$(svn info | grep URL | grep smewapp)
if [ "$is_smewapp" != "" ]
then
  echo "This is the framework itself, not updating..."
  exit 1
fi

### go to this dir
cd $(dirname $0)

### get the framework version used by the application
. framework_rev.txt
last_rev=$curr_rev

### get the svnroot
svn update ../../smewapp
svnurl=$(svn info ../../smewapp | grep URL | cut -d' ' -f2)
svnroot=${svnurl%/smewapp*}

### get the current version of the framework
curr_rev=$(svn info ../../smewapp/ | grep 'Last Changed Rev' | cut -d' ' -f4)

### merge changes from the last revision to the current 
### revision of the framework, into the application
svn merge $svnroot/smewapp/trunk/@{$last_rev,$curr_rev} ..

### save the framework version
cat <<EOF > framework_rev.txt
last_rev=$last_rev
curr_rev=$curr_rev
EOF

### make executable any shell files that is updated
find .. -name '*.sh' | xargs chmod +x

### resolve any conflicts and commit
echo
echo "Now resolve any conflicts and commit the changes."
echo

