#!/bin/bash
### generate a list of modules from the directory tree

tree -d ../modules/ | gawk '$NF!="CVS"' > modules.txt
