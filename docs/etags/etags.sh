#!/bin/bash

web_app_dir=../../../phpwebapp/web_app/
find $web_app_dir \
     -name '*.php'  -o -name '*.js' -o -name '*.html' -o -name '*.db' \
 | etags --output=WEB_APP_ETAGS \
         --language=none \
         --regex=@web_app.etags \
         -

find ../.. \
     -name '*.php'  -o -name '*.js' -o -name '*.html' -o -name '*.db' \
 | etags --output=APP_ETAGS \
         --language=none \
         --include=WEB_APP_ETAGS \
         --regex=@web_app.etags \
         -
