<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Functions and variables that are global
 * and are used through the whole application.
 */

/**
 * Returns the current date in the requested format.
 */
function get_curr_date($format)
{
  if (TEST) 
    {
      return simulated_date($format);
    }
  else
    {
      return date($format);
    }
}

/**
 * Converts a month number to format MMM, e.g. from 1 to Jan.
 */
function int2mon($m_id)
{
  $mon = date("M", mktime(0,0,0,$m_id,1,2002));
  return $mon;
}

/** 
 * Converts a MMM month format to integer, e.g. from Jan to 1. 
 */
function mon2int($mon)
{
  return date("n", strtotime("$mon 01, 2002"));
}


function simulated_date($format)
{
  $day   = WebApp::getSVar("simulated_current_day");
  $month = WebApp::getSVar("simulated_current_month");
  $year  = WebApp::getSVar("simulated_current_year");

  switch ($format)
    {
    case "Y":
      return $year;
      break;
    case "m":
      return $month;
      break;
    case "M":
      return int2mon($month);
      break;
    case "n":
      return (int)$month;
      break;
    case "d":
      return $day;
      break;
    case "d/m/Y":
      return "$day/$month/$year";
      break;
    case "Y-m-d":
      return "$year-$month-$day";
    case "M d, Y":
      return int2mon($month)." $day, $year";
      break;
    }
}

/** Execute a shell command, checking it first for invalid input. */
function shell($cmd)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd .= ' 2>&1';
  $output = shell_exec($cmd);
  //print "<xmp>$cmd \n----- \n$output</xmp>\n";  //debug
  return $output;
}

/** Set the owner and group of the file to WWW_DATA_OWNER and WWW_DATA_GROUP */
function set_data_owner($fname)
{
  shell("./chown.sh $fname");
}

/** Write the content to the file. */
function write_file($fname, &$fcontent)
{
  //create the directories in the path, in case they do not exist
  shell('mkdir -p '.dirname($fname));

  //write the content to the file
  $fp = fopen($fname, 'w');
  fputs($fp, $fcontent);
  fclose($fp);

  //set proper owner and permissions
  set_data_owner($fname);
}

/**
 * Generate a latex file from the given latex template and variables,
 * then convert it to pdf and open the pdf file. It is used to generate
 * nicely printable documents and reports.
 */
function get_pdf_file($latex_tpl, $vars)
{
  //get tplname
  $tplname = basename($latex_tpl);
  $tplname = ereg_replace('\\.tex$', '', $tplname);
  $uid = WebApp::getSVar('u_id');

  //fill the latex template and write it to a file
  $latex_content = WebApp::fill_template($latex_tpl, $vars);
  write_file("tmp/${tplname}_${uid}.tex", $latex_content);

  //convert the latex file to pdf
  shell_exec("cd tmp/; pdflatex -interaction=batchmode ${tplname}_${uid}.tex");
  set_data_owner('tmp/${tplname}_${uid}.*');

  //open the pdf file in a popup window
  $pdf_file = "tmp/${tplname}_${uid}.pdf";
  $html_content = file_get_contents(TPL.'etc/get_pdf_popup.html');
  $window_features = 'width=200,height=30';
  $timeout = 5000;
  WebApp::popup_window('pdf', $pdf_file, $html_content,
                       $window_features, $timeout);
}
?>