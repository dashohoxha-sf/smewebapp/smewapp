
- Write the smewebapp_manual, what is the framework how it is used,
  what each module-template is for, its features etc.

- Generate code documentation.

- Upload at SF and make a release.

- Improve the generation of the PDF format reports (printables).
  Try to generate reports in the XLS (spreadsheet) format as well.

- Try to use PostgreSQL or some other object-relational or
  object-oriented databases (e.g. cache).

---------------------------------------------------------------

Create a new project for ilar
-----------------------------

* clean the modules and make them functional

* merge in the changes of the latest ilar

* improve the stylesheets

* document the architecture, design, usage of the application etc.

* try to abstract anything 
  and to automate somehow  the creation of new modules

---------------------------------------------------------------
Misc
----
- Clean the modules of 'printings'.

- Refresh the analize/design diagrams and the databaze diagram.

* print: 
  Create the latex templates of the following documents,
  which are used for generating PDF versions of the documents,
  (which are nice for both printing and sending by email as attachments)
    - printings/contracts/print/contract/contract.tex
    - printings/tcards/print/tcardPrint.html
    - printings/tcards/print/tcard/tcard.tex

* Learn:
  - latex
  - postgresql

* Create web interface wizards for creating new modules
  from module templates.

---------------------------------------------------------------
Clean the Modules and Make Them Functional
--------------------------------------------
  
- printings
  - porosi
  - gjendje karte
  - ekonomiku

- merge databases of nevila and albana

- magazina

- buletinet

- raportet

- modules init_db, edit_tables, backup_restore (admin/superuser)


- replace select() by iface()

- modify access_rights.php:modify_access_rights().

- clean the cases at: print/print_interface.php:on_select():switch

- replace SMEWeb with SMEWebApp in all the comments

---------------------------------------------------------------
Merge in the Changes of the Latest ilar
---------------------------------------

- Find the modules that have changed and apply the changes manually.


---------------------------------------------------------------
Translate Everything to English and Mark Translatable Messages
--------------------------------------------------------------

- i18n/l10n (translation) for each module
- translate only the parts that are generic and reusable,
  leave untranslated the parts that are specific to ilar

---------------------------------------------------------------
Improve the Stylesheets
-----------------------

- 


---------------------------------------------------------------
Document the Architecture, Design and Usage of the Application
--------------------------------------------------------------

- Update the database, use-cases and design diagrams
  in the UML model.


---------------------------------------------------------------
Abstract and Automate Anything 
------------------------------

- automate the construction of new filters from template filters
  (filter_1 or filter_2) and then construct clean copies of the
  filters: faturat, kartat, mallrat, punet, zerat

- create the module: help

- create module templates: input-form, form-with-list,
  simple-list, list, pdf_document, etc.


---------------------------------------------------------------
Bugs
----

- There are problems with SELinux, e.g. edit_menu does not work.
- global.php:get_pdf_file(): WebApp::popup_window()
  JS error: unterminated string literal.


---------------------------------------------------------------
Done
----

- save the passwords encrypted and check for an encrypted password
  at modules: user_profile, users, login, superuser/change_password
      srand(time());
      $passwd = crypt($email_password, rand());
      $valid = ($crypted_passwd == crypt($passwd, $crypted_passwd));

- modules of admin and superuser

- renamed filter/data to filter/date and cleaned it,
  added it at module_templates as filter_2

- cleaned filter/fBuletinet and added it at module_templates as filter_1

- created the module template pdf_document

- created module template 'documents' and the script for creating
  a module from the module template

- In 'tools/', the last revision and the current revision
  are kept in a single file, like this:
  --code
  last_rev=23
  curr_rev=27
  ----

- Created from module templates a translation database (compendium)
  for each language, in order to facilitate the translation of the
  new modules.

- The framework contains a demo module for each module template
  which demonstrates how the module can be used.

- Created a DocBookWiki help book about the SMEWebApp framework, and
  linked each demonstration module to the corresponding module template.

- Created a compendium for the translations.

- Added a menu for selecting the languages.
