<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net
*/
define("DBHOST", "localhost");
define("DBUSER", "root");
define("DBPASS", "");
define("DBNAME", "smewapp");
?>
