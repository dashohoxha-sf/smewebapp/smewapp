<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net
*/

//constants of the paths in the application
define('WEBAPP_PATH', UP_PATH.'web_app/');

define('GRAPHICS', 'graphics/');
define('CSS',      'css/');
define('TPL',      'modules/');
define('TPL_PATH', 'modules/');
define('FILTER',   TPL.'filter/');
define('MENU',     TPL.'menu/');
define('TABS',     'webobjects/tabs/');
?>