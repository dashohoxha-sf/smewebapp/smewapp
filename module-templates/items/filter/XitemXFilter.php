<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XitemXFilter extends WebObject
{
  function init()
    {
      $this->addSVars(array(
                            'name'     => '',
                            'unit'     => '',
                            'quantity' => '',
                            'price'    => '',
                            'value'    => ''
                            ));
      $this->addSVar('filter_condition', '(1=1)');
    }

  function onParse()
    {
      $this->setSVar('filter_condition', $this->get_filter_condition());
    }

  function get_filter_condition()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      $conditions = array();

      if ($name!='')
        $conditions[] = "name LIKE '%$name%'";

      if ($unit!='')
        $conditions[] = "unit = '$unit'";

      if ($quantity!='')
        {
          $ch = $quantity[0];
          if ($ch!='=' and $ch!='<' and $ch!='>') $quantity = '='.$quantity;
          $conditions[] = "quantity $quantity";
        }

      if ($price!='')
        {
          $ch = $price[0];
          if ($ch!='=' and $ch!='<' and $ch!='>') $price = '='.$price;
          $conditions[] = "price $price";
        }

      if ($value!='')
        {
          $ch = $value[0];
          if ($ch!='=' and $ch!='<' and $ch!='>') $value = '='.$value;
          $conditions[] = "value $value";
        }

      $filter = implode(' AND ', $conditions);
      if ($filter=='') $filter = '(1=1)';
      else $filter = "($filter)";

      return $filter;
    }
}
?>
