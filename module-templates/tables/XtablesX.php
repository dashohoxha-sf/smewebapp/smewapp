<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    XpackageX
 * @subpackage XtablesX
 */
class XtablesX extends WebObject
{
  function init()
    {
      //there may be different groups of tables for different interfaces
      $this->addSVar('group', 'group1');  //group1 | group2 | group3 etc. 
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_interface($event->args);
        }
    }

  function select_interface($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
        default:
        case 'XpackageX/XtablesX/group1':
          $interface_title = T_("XtablesX").'-->'.T_("Group1");
          $group = 'group1';
          break;
        case 'XpackageX/XtablesX/group2':
          $interface_title = T_("XtablesX").'-->'.T_("Group2");
          $group = 'group2';
          break;
        case 'XpackageX/XtablesX/group3':
          $interface_title = T_("XtablesX").'-->'.T_("Group3");
          $group = 'group3';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('group', $group);
    }
}
?>