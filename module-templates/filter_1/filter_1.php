<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package filter
 * @subpackage buletinet
 */
class filter_1 extends WebObject
{
  function init()
  {
    $this->addSVars( array(
                           "makina" => "",
                           "makinisti" => "",
                           "turni" => ""
                           ) );
    $this->addSVar("filter", "");
  }
    
  function onParse()
  {
    $this->setSVar("filter", $this->get_filter());
  }  
    
  function get_filter()
  {
    $makina    = $this->getSVar("makina");
    $makinisti = $this->getSVar("makinisti");
    $turni     = $this->getSVar("turni");
      
    $conditions = array();
    if ($makina<>"")
      $conditions[] = "makina='$makina'";

    if ($makinisti<>"")
      $conditions[] = "makinisti LIKE '%".$makinisti."%'";
      
    if ($turni<>"")
      $conditions[] = "turni='$turni'";

    $filter = implode(" AND ", $conditions);
    if ($filter<>"")    $filter = "(".$filter.")";

    return $filter;
  }
}
?>