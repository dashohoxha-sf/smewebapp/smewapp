#!/bin/bash
### Create a new module by copying the module template 
### and replacing the variables in it.

if [ "$1" = "" ]
then
  echo "Usage: $0 destination"
  echo "Create a new module by copying the module template to the"
  echo "given destination directory, and replacing the variables in it."
  exit 0
fi

template=$(dirname $0)
destination=$1

### get the values of the variables
. $template/get_vars.sh

### export a copy of the module template from the repository
svn_url=$(svn info $(dirname $0) | grep 'URL: ' | cut -d ' ' -f 2)
svn export $svn_url $destination --force

### rename files
files=$(find $destination -name '*XdocumentsX*')
for file in $files
do
  new_file=${file//XdocumentsX/$documents}
  mv $file $new_file
done
files=$(find $destination -name '*XdocumentX*')
for file in $files
do
  new_file=${file//XdocumentX/$document}
  mv $file $new_file
done
files=$(find $destination -name '*XpackageX*')
for file in $files
do
  new_file=${file//XpackageX/$package}
  mv $file $new_file
done

### replace module variables
find $destination -type f \
     | xargs sed -i \
             -e s/XappnameX/$appname/g     \
             -e s/XpackageX/$package/g     \
             -e s/XmoduleX/$module/g       \
             -e s/XdbtableX/$dbtable/g     \
             -e s/XdocumentX/$document/g   \
             -e s/XdocumentsX/$documents/g

### save variables
cat <<EOF > $destination/vars
appname=$appname
package=$package
module=$module
dbtable=$dbtable
documents=$documents
document=$document
EOF

### remove create_module.sh
rm $destination/create_module.sh
rm $destination/get_vars.sh
rm $destination/summary_reports.txt


