<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package admin
 * @subpackage users
 */
class userEdit extends formWebObj
{
  var $user_data =  array( 
                          'username'     => '',
                          'firstname'    => '',
                          'lastname'     => '',
                          'title'        => '',
                          'phone1'       => '',
                          'phone2'       => '',
                          'salary'       => '0',
                          'e_mail'       => '',
                          'address'      => '',
                          'notes'        => '',
                          'access_rights'=> ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new user in database */
  function on_add($event_args)
    {
      $this->user_data = array_merge($this->user_data, $event_args);

      //check that such a username does not exist in DB
      $username = $event_args['username'];
      $rs = WebApp::openRS('getUserID', array('username'=>$username));
      if (!$rs->EOF())
        {
          $msg = "The username '$username' is already used for \n"
            . "somebody else.  Please choose another username.";
          WebApp::message($msg);
          $this->user_data['username'] = '';
          return;
        }

      //add the user
      WebApp::execDBCmd('addUser', $this->user_data);

      //get the id of the just added user
      $rs = WebApp::openRS('getUserID', array('username'=>$username));
      $user_id = $rs->Field('user_id');

      //set the new user as current user and change the mode to edit
      WebApp::setSVar('userList->currentUser', $user_id);
      $this->setSVar('mode', 'edit');

      $this->update_password($event_args);
    }

  function update_password($event_args)
    {
      //if the password is given, set the password of the new user
      $password = $event_args['password'];
      $password = trim($password);
      if ($password=='')  return;
  
      //encrypt the given password
      srand(time());
      $password = crypt($password, rand());

      //save the encrypted password
      WebApp::execDBCmd('updatePassword', array('password'=>$password));
    }

  /** delete the current user */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('deleteUser');

      //the currentUser is deleted,
      //set current the first user in the list
      $userList = WebApp::getObject('userList');
      $userList->selectFirst();

      //acknowledgment message
      WebApp::message('User deleted.');
    }

  /** save the changes */
  function on_save($event_args)
    {
      //get the query id, according to the edit_mode
      $edit_mode = WebApp::getSVar('users->edit_mode');
      $edit_mode = ucfirst($edit_mode);
      $update_query = 'update'.$edit_mode;

      //update user data
      WebApp::execDBCmd($update_query, $event_args);

      $this->update_password($event_args);
    }

  function onParse()
    {
      //get the current user from the list of users
      $user = WebApp::getSVar('userList->currentUser');

      if ($user==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $user_data = $this->user_data; 
        }
      else
        {
          $rs = WebApp::openRS('currentUser');
          $user_data = $rs->Fields();
        }
      WebApp::addVars($user_data);      
    }
}
?>