// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function add()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.userEdit);
  SendEvent('userEdit', 'add', event_args);
}

function save()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.userEdit);
  SendEvent('userEdit', 'save', event_args);
}

function del()
{
  var msg = "You are deleting the current user.";
  if (!confirm(msg))  return;
  SendEvent('userEdit', 'delete');
}

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.userEdit;
  var username = form.username.value;

  //username should not be empty, otherwise 
  //the user cannot be accessed anymore
  username = username.replace(/ +/, '');
  if (username=='')
    {
      alert("Please fill the username field.");
      form.username.focus();
      return true;
    }
    
  return false;
}
