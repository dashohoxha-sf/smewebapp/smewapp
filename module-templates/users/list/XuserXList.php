<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XuserXList extends WebObject
{
  function init()
    {
      $this->addSVar("current_XuserX", UNDEFINED);      
      //set current the first XuserX in the list
      $this->selectFirst();  
    }

  /** set current_XuserX as the first XuserX in the list */
  function selectFirst()
    {  
      $rs = WebApp::openRS("selected_XusersX");
      $first_XuserX = $rs->Field("XuserX_id");
      $this->setSVar("current_XuserX", $first_XuserX);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("selected_XusersX->current_page", $page);
      $this->selectFirst();  
    }

  function on_refresh($event_args)
    {
      //recount the selected XusersX
      WebApp::setSVar("selected_XusersX->recount", "true");

      //select the first one in the current page
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      //set current_XuserX to the selected one
      $rs = WebApp::openRS("get_XuserX", $event_args);
      if ($rs->EOF())
        {
          $this->selectFirst();   
        }
      else
        {
          $XuserX = $event_args["XuserX_id"];
          $this->setSVar("current_XuserX", $XuserX);
        }
    }

  function on_add_new_XuserX($event_args)
    {
      //when the button Add New XuserX is clicked
      //make current_XuserX UNDEFINED
      $this->setSVar("current_XuserX", UNDEFINED);
    }

  function onParse()
    {
      //recount the selected XusersX
      WebApp::setSVar("selected_XusersX->recount", "true");
    }
}
?>