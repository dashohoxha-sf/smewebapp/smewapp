
DROP TABLE IF EXISTS XdbtableX;
CREATE TABLE XdbtableX (
  XuserX_id int(11) NOT NULL auto_increment,
  username varchar(20) default NULL,
  password varchar(20) NOT NULL default '',
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  salary smallint(6) default NULL,
  title varchar(50) default NULL,
  access_rights text,
  PRIMARY KEY  (XuserX_id),
  UNIQUE KEY u (username)
) TYPE=MyISAM;

