
### get the values of the variables

if [ -f $destination/vars ]
then
  . $destination/vars
fi

if [ -z "$appname" ]
then
  read -p "appname[appname]=" appname
  appname=${appname:-appname}
fi

if [ -z "$package" ]
then
  read -p "package[package]=" package
  package=${package:-package}
fi

if [ -z "$module" ]
then
  read -p "module[module]=" module
  module=${module:-module}
fi

if [ -z "$interface" ]
then
  read -p "interface[$module]=" interface
  interface=${interface:-$module}
fi

if [ -z "$dbtable" ]
then
  read -p "dbtable[$module]=" dbtable
  dbtable=${dbtable:-$module}
fi

if [ -z "$user" ]
then
  read -p "user[user]=" user
  user=${user:-user}
fi

if [ -z "$users" ]
then
  read -p "users[users]=" users
  users=${users:-users}
fi

