<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XuserXEdit extends formWebObj
{
  var $XuserX_data =  array( 
                          'username'     => '',
                          'firstname'    => '',
                          'lastname'     => '',
                          'title'        => '',
                          'phone1'       => '',
                          'phone2'       => '',
                          'salary'       => '0',
                          'e_mail'       => '',
                          'address'      => '',
                          'notes'        => '',
                          'access_rights'=> ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new XuserX in database */
  function on_add($event_args)
    {
      $this->XuserX_data = array_merge($this->XuserX_data, $event_args);

      //check that such a username does not exist in DB
      $username = $event_args['username'];
      $rs = WebApp::openRS('get_XuserX_id', array('username'=>$username));
      if (!$rs->EOF())
        {
          $msg = T_("The username 'var_username' is already used for \n"
            . "somebody else.  Please choose another username.");
          $msg = str_replace('var_username', $username, $msg);
          WebApp::message($msg);
          $this->XuserX_data['username'] = '';
          return;
        }

      //add the XuserX
      WebApp::execDBCmd('add_XuserX', $this->XuserX_data);

      //get the id of the just added XuserX
      $rs = WebApp::openRS('get_XuserX_id', array('username'=>$username));
      $XuserX_id = $rs->Field('XuserX_id');

      //set the new XuserX as current XuserX and change the mode to edit
      WebApp::setSVar('XuserXList->current_XuserX', $XuserX_id);
      $this->setSVar('mode', 'edit');

      $this->update_password($event_args);
    }

  function update_password($event_args)
    {
      //if the password is given, set the password of the new XuserX
      $password = $event_args['password'];
      $password = trim($password);
      if ($password=='')  return;
  
      //encrypt the given password
      srand(time());
      $password = crypt($password, rand());

      //save the encrypted password
      WebApp::execDBCmd('updatePassword', array('password'=>$password));
    }

  /** delete the current XuserX */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('delete_XuserX');

      //the current_XuserX is deleted,
      //set current the first XuserX in the list
      $XuserXList = WebApp::getObject('XuserXList');
      $XuserXList->selectFirst();

      //acknowledgment message
      WebApp::message('XuserX deleted.');
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update XuserX data
      WebApp::execDBCmd('update_XuserX', $event_args);

      $this->update_password($event_args);
    }

  function onParse()
    {
      //get the current XuserX from the list of XusersX
      $XuserX = WebApp::getSVar('XuserXList->current_XuserX');

      if ($XuserX==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $XuserX_data = $this->XuserX_data; 
        }
      else
        {
          $rs = WebApp::openRS('current_XuserX');
          $XuserX_data = $rs->Fields();
        }
      WebApp::addVars($XuserX_data);      
    }
}
?>