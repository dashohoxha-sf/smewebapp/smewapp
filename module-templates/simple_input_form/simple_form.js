// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function save()
{
  var event_args;
  var form = document.simple_form;
  var firstname = form.firstname.value;
  var lastname = form.lastname.value;
  var phone1 = form.phone1.value;
  var e_mail = form.e_mail.value;
  var address = form.address.value;
  var args = new Array();

  //check that e_mail is not empty
  if (e_mail=='')
    {
      alert('Please give the e-mail.');
      form.e_mail.focus();
      return;
    }

  args.push('firstname=' + firstname);
  args.push('lastname=' + lastname);
  args.push('phone1=' + phone1);
  args.push('e_mail=' + e_mail);
  args.push('address=' + encode_arg_value(address));

  event_args = args.join(';');
  SendEvent('simple_form', 'save', event_args);
}
