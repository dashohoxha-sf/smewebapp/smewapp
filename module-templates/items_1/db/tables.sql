
DROP TABLE IF EXISTS XdbtableX;
CREATE TABLE XdbtableX (
  XitemX varchar(20) NOT NULL,
  unit varchar(10) NOT NULL default '',
  price smallint(6) default NULL,
  type varchar(20) default NULL,  -- ingredient or resell item
  partners text,  -- whom this item is purchased from (list of partner ids)
  notes text,
  PRIMARY KEY  (XitemX)
) TYPE=MyISAM;

