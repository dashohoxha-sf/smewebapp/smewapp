// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function add_XitemX()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.XitemXEdit);
  SendEvent('XitemXEdit', 'add', event_args);
}

function save_XitemX()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.XitemXEdit);
  SendEvent('XitemXEdit', 'save', event_args);
}

function del_XitemX()
{
  var msg = T_("You are deleting the current XitemX.");
  if (!confirm(msg))  return;
  SendEvent('XitemXEdit', 'delete');
}

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.XitemXEdit;
  var XitemX = form.XitemX.value;

  //XitemX should not be empty, otherwise 
  //the XitemX cannot be accessed anymore
  XitemX = XitemX.replace(/ +/, '');
  if (XitemX=='')
    {
      alert(T_("Please fill the XitemX field."));
      form.XitemX.focus();
      return true;
    }
    
  return false;
}
