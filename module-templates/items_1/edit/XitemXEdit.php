<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XitemXEdit extends formWebObj
{
  var $XitemX_data =  array( 
                          'XitemX'   => '',
                          'unit'     => '',
                          'price'    => '',
                          'type'     => '',
                          'partners' => '',
                          'notes'    => ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new XitemX in database */
  function on_add($event_args)
    {
      $this->XitemX_data = array_merge($this->XitemX_data, $event_args);

      //check that such an XitemX does not exist in DB
      $XitemX = $event_args['XitemX'];
      $rs = WebApp::openRS('get_XitemX', array('XitemX'=>$XitemX));
      if (!$rs->EOF())
        {
          $msg = T_("The XitemX 'var_XitemX' is already used for \n"
            . "somebody else.  Please choose another XitemX.");
          $msg = str_replace('var_XitemX', $XitemX, $msg);
          WebApp::message($msg);
          $this->XitemX_data['XitemX'] = '';
          return;
        }

      //add the XitemX
      WebApp::execDBCmd('add_XitemX', $this->XitemX_data);

      //set the new XitemX as selected and change the mode to edit
      WebApp::setSVar('XitemXList->selected', $XitemX);
      $this->setSVar('mode', 'edit');
    }

  /** delete the current XitemX */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('delete_XitemX');

      //the selected XitemX is deleted,
      //select the first XitemX in the list
      $XitemXList = WebApp::getObject('XitemXList');
      $XitemXList->select_first();

      //acknowledgment message
      WebApp::message(T_("Item deleted."));
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update XitemX data
      WebApp::execDBCmd('update_XitemX', $event_args);
    }

  function onParse()
    {
      //get the current XitemX from the list of XitemsX
      $XitemX = WebApp::getSVar('XitemXList->selected');

      if ($XitemX==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $XitemX_data = $this->XitemX_data; 
        }
      else
        {
          $rs = WebApp::openRS('selected');
          $XitemX_data = $rs->Fields();
        }
      WebApp::addVars($XitemX_data);      
    }
}
?>