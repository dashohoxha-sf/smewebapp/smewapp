
DROP TABLE IF EXISTS XdbtableX;
CREATE TABLE XdbtableX (
  XproductIdX varchar(20) NOT NULL,
  XitemIdX varchar(20) NOT NULL,
  unit varchar(10) default NULL,
  quantity int(11) default NULL,
  PRIMARY KEY  (XproductIdX, XitemIdX)
) TYPE=MyISAM
COMMENT='The table that contains some XitemIdX for each XproductIdX.';

