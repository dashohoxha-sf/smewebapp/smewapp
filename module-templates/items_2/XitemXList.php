<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */

class XitemXList extends WebObject
{
  function init()
    {
      //the value of the field that is used as a foreign key
      $this->addSVar('XproductIdX', UNDEFINED);
    }

  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('XitemsX_rs->current_page', $page);
    }
    
  function on_del($event_args)
    {
      WebApp::execDBCmd('del_XitemX', $event_args);
    } 
    
  function on_add($event_args)
    {
      WebApp::setSVar('XitemXEdit->mode', 'add');
      WebApp::setSVar('XitemXEdit->XitemIdX', UNDEFINED);
    } 

  function on_edit($event_args)
    {
      WebApp::setSVar('XitemXEdit->mode', 'edit');
      WebApp::setSVar('XitemXEdit->XitemIdX', $event_args['XitemIdX']);
    } 

  function onRender()
    {
      WebApp::setSVar('XitemsX_rs->recount', 'true');
    }
}
?>


