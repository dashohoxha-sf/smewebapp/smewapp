
### get the values of the variables

if [ -f $destination/vars ]
then
  . $destination/vars
fi

if [ -z "$appname" ]
then
  read -p "appname[appname]=" appname
  appname=${appname:-appname}
fi

if [ -z "$package" ]
then
  read -p "package[package]=" package
  package=${package:-package}
fi

if [ -z "$module" ]
then
  read -p "module[items]=" module
  module=${module:-items}
fi

if [ -z "$dbtable" ]
then
  read -p "dbtable[$module]=" dbtable
  dbtable=${dbtable:-$module}
fi

if [ -z "$item" ]
then
  read -p "item[item]=" item
  item=${item:-item}
fi

if [ -z "$items" ]
then
  read -p "items[items]=" items
  items=${items:-items}
fi

if [ -z "$itemId" ]
then
  read -p "itemId[${item}_id]=" itemId
  itemId=${itemId:-${item}_id}
fi

if [ -z "$productId" ]
then
  read -p "productId[product_id]=" productId
  productId=${productId:-product_id}
fi
