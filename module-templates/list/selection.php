<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package printings
* @subpackage contracts
*/

class selection extends WebObject
{
  function onRender()
    {
      $day1 = WebApp::getSVar("date->day1");
      $month1 = WebApp::getSVar("date->month1");
      $year1 = WebApp::getSVar("date->year1");
      $day2 = WebApp::getSVar("date->day2");
      $month2 = WebApp::getSVar("date->month2");
      $year2 = WebApp::getSVar("date->year2");
      $period = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar("period", $period);


      //nderto selektimin e bere ne baze te te dhenat e tjera
      $fields = "";
      $autorizimi = WebApp::getSVar("fPrintings->autorizimi");
      $printing_title = WebApp::getSVar("fPrintings->printing_title");
      $porositesi = WebApp::getSVar("fPrintings->porositesi");
      $printing_format = WebApp::getSVar("fPrintings->printing_format");

      $arr_fields = array();
      if ($autorizimi!='')
        $arr_fields[] = "Autorizimi='$autorizimi'";
      if ($printing_title!='')
        $arr_fields[] = "Title contains '$printing_title'";
      if ($porositesi!='')
        $arr_fields[] = "Porositesi contains '$porositesi'";
      if ($printing_format!='')
        $arr_fields[] = "Format contains '$printing_format'";

      $fields = implode(' AND ', $arr_fields);
      WebApp::addVar("fields", $fields);
    }
}
?>