<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package printings
* @subpackage contracts
*/

class contractList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("contracts_rs->current_page", $page);
    }

  function onRender()
    {
      WebApp::setSVar("contracts_rs->recount", "true");

      $filter_condition = $this->get_filter_condition();
      WebApp::addVar("filter_condition", $filter_condition);
    }

  function get_filter_condition()
    {
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "data", $date_filter);
      $contracts_filter = WebApp::getSVar("fPrintings->filter");

      $conditions[] = $date_filter;
      if ($contracts_filter<>"")   $conditions[] = $contracts_filter;

      $filter_condition = implode(" AND ", $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
    
  function on_rm($event_args)
    {
      WebApp::execDBCmd("rmcontract", $event_args);
      
    } 
}
?>


