<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<Recordset ID="listbox::makina">
  <Query>
    SELECT makina AS id, makina AS label
    FROM makinat
  </Query>
</Recordset>

<Recordset ID="listbox::gjinia">
  <Query>
    SELECT gjinia AS id,  gjinia AS label
    FROM gjinia
  </Query>
</Recordset>

<Recordset ID="listbox::permasa1">
  <Query>
    SELECT permasa1 AS id, permasa1 AS label
    FROM permasat_leter
  </Query>
</Recordset>

<Recordset ID="listbox::permasa2">
  <Query>
    SELECT permasa2 AS id, permasa2 AS label
    FROM permasat_leter
  </Query>
</Recordset>

<Recordset ID="listbox::pesha">
  <Query>
    SELECT pesha AS id, pesha AS label
    FROM pesha_leter
    </Query>
</Recordset>

<Recordset ID="listbox::lloji">
  <Query>
    SELECT lloji AS id, lloji AS label
    FROM lloji_leter
    </Query>
</Recordset>
