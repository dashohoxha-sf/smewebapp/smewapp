<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package printings
 * @subpackage contracts
 */

include_once FORM_PATH."formWebObj.php";

class contractEdit extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      $this->addSVar("printing_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getContract");
          $close_date = $rs->Field("close_date");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add empty variables for each field in the table
          $rs = WebApp::execQuery("SHOW FIELDS FROM printings");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));

          //title
          $title = "New Contract";
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getContract");
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $autorizim = $fields['autorizim_nr'];
          $printing_title = $fields['printing_title'];
          $title = "Contract: $autorizim: $printing_title";
        }

      WebApp::addVar("title", $title);
    }

  function on_close($event_args)
    {
      $params["close_date"] = get_curr_date("Y-M-D");
      WebApp::execDBCmd("closeContract", $params);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      $params = $event_args;
      $params["printing_format"] = $this->get_printing_format($event_args);
      if ($mode=="add")
        {
          $this->add_new_contract($params);
        }
      else //mode=="edit"
        {
          WebApp::execDBCmd("updateContract", $params);
        }
    }

  function get_printing_format($event_args)
    {
      $permasa1 = $event_args["permasa1"];
      $permasa2 = $event_args["permasa2"];
      $pesha = $event_args["pesha"];
      $lloji = $event_args["lloji"];
      $lloj_pune = $event_args["lloj_pune"];
      //formati vetem per lloj pune shtyp
       
      $printing_format= $permasa1."/".$permasa2."/".$pesha."gr/".$lloji;
      //WebApp::debug_msg($printing_format, "printing_format: ");
      return $printing_format;
    }

  function add_new_contract($params)
    {
      //find a printing_id for the new project
      $rs = WebApp::openRS("getLastContractId");
      $last_printing_id = $rs->Field("last_printing_id");
      $printing_id = (($last_printing_id==UNDEFINED) ? 1 : $last_printing_id+1);

      //add the new contract
      $params["printing_id"] = $printing_id;
      $params["data"] = get_curr_date("Y-m-d");
      
      
      WebApp::execDBCmd("newContract", $params);

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'contracts->recount' to "true"
      //so that the records are counted again
      //and the new contract is displayed at the end of the list
      WebApp::setSVar("contracts->recount", "true");

      //set the new printing_id
      $this->setSVar("printing_id", $printing_id);
    }
}
?>





