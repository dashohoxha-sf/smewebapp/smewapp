// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function date_not_in_future(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = arr_date[0];
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date > curr_date)
    {
      alert("Warning: The selected date is later than the current date.");
      //set_current_date(txtbox);
    }
}

function date_not_in_past(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = (arr_date[0]*1) + 1;
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date < curr_date)
    {
      alert("Kujdes: Data e zgjedhur nga ju �sht� von� sesa data aktuale.");
      //set_current_date(txtbox);
    }
}

function set_current_date(txtbox)
{
  var curr_date = get_curr_date();
  var day = curr_date.getDate();
  var month = curr_date.getMonth() + 1;
  var year = curr_date.getFullYear();

  if (day < 10)    day   = "0" + day;
  if (month < 10)  month = "0" + month;
  str_date = day + '/' + month + '/' + year;
  txtbox.value = str_date;
}

function save()
{
  var form = document.contractEdit;
  if (!validate(form))  return;
  
  var event_args = getEventArgs(form);
   
  var mode = session.getVar("contractEdit->mode");
  if (mode=="add")  saveFormData(form);
  SendEvent("contractEdit", "save", event_args);
}

function close_doc()
{
  var confirm_msg;
  confirm_msg = "Jeni duke mbyllur Botimin, \n"
    + "k�shtu q� nuk mund t� b�ni m� ndryshime n� t�.\n\n\n"
    + "Jeni t� sigurt se doni ta mbyllni k�t� Botim?\n\n";
  if (confirm(confirm_msg))
    {
      GoTo("thisPage?event=contractEdit.close");
    }
}

function editable()
     //returns false if the document is closed
{
  var closed = session.getVar("contractEdit->closed");

  if (closed=="true")
    {
      alert("Ky Botim �sht� i mbyllur dhe nuk mund t� modifikohet.");
      return false;
    }

  return true;
}



function validate(form)
{
  if (form.autorizim_nr.value=="")
    {
      alert ("Ju lutem mos e lini bosh 'Autorizimi'.");
      form.autorizim_nr.focus();
      return false;
    }
    
  if (form.gjinia.value=="")
    {
      alert ("Ju lutem mos e lini bosh 'Gjinia'.");
      form.gjinia.focus();
      return false;
    }
  if (form.kopjet.value=="0")
    {
      alert ("Ju lutem mos e lini bosh 'Tirazhi'.");
      form.kopjet.focus();
      return false;
    }    
  if (form.kopjet.value=="")
    {
      alert ("Ju lutem mos e lini bosh 'Tirazhi'.");
      form.kopjet.focus();
      return false;
    }    
      
  if (form.printing_title.value == "")
    {
      alert ("Ju lutem mos e lini bosh 'Titulli i Botimit'.");
      form.printing_title.focus();
      return false;
    }    
  
  if (form.shpenzimetp.value == "0")
    {
      alert ("Ju lutem vendosni 'Shpenzimet e parashikuara'.");
      form.shpenzimetp.focus();
      return false;
    }            
   
  return true;
}









