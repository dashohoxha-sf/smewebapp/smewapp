<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XdocumentXEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('XdocumentX_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_XdocumentX');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $XdocumentX = $this->pad_record(array(), 'XdbtableX');
          WebApp::addVars($XdocumentX);

          //title
          $title = T_("New XdocumentX");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_XdocumentX');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $XdocumentX_id = $fields['XdocumentX_id'];
          $title = T_("XdocumentX").": $XdocumentX_id";
          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['XdocumentX_date']=='')
        $record['XdocumentX_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_XdocumentX($record);
        }
      else //mode=='edit'
        {
          $record['XdocumentX_id'] = $this->getSVar('XdocumentX_id');
          $this->update_record($record, 'XdbtableX', 'XdocumentX_id');
        }
    }

  /** create a new XdocumentX with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_XdocumentX');
      $record = $rs->Fields();
      unset($record['XdocumentX_id']);
      $this->add_new_XdocumentX($record);
    }

  function on_close($event_args)
    {
      $params['close_date'] = get_curr_date('Y-M-D');
      WebApp::execDBCmd('close_XdocumentX', $params);
    }

  function add_new_XdocumentX($XdocumentX)
    {
      //add the new XdocumentX
      $XdocumentX['close_date'] = 'NULL';
      $this->insert_record($XdocumentX, 'XdbtableX');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'XdocumentsX_rs->recount' to 'true'
      //so that the records are counted again
      //and the new XdocumentX is displayed at the end of the list
      WebApp::setSVar('XdocumentsX_rs->recount', 'true');

      //set the new XdocumentX_id
      $rs = WebApp::openRS('get_last_XdocumentX_id');
      $XdocumentX_id = $rs->Field('last_XdocumentX_id');
      $this->setSVar('XdocumentX_id', $XdocumentX_id);
    }
}
?>