// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function view_XdocumentX(XdocumentX_id)
{
  select('XpackageX/XmoduleX/edit', 'XdocumentX_id='+XdocumentX_id);
}

function print_XdocumentX(XdocumentX_id)
{
  print_template('XpackageX/XmoduleX/edit', 'XdocumentX_id='+XdocumentX_id);
}

function del_XdocumentX(XdocumentX_id, nr)
{
  var msg = T_("Deleting the XdocumentX nr") + ': ' + nr;
  if (!confirm(msg))  return;

  SendEvent('XdocumentXList', 'del', 'XdocumentX_id='+XdocumentX_id);
}
