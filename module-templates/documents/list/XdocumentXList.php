<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    XpackageX
 * @subpackage XmoduleX
 */
class XdocumentXList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("XdocumentsX_rs->current_page", $page);
    }
    
  function on_del($event_args)
    {
      $rs = WebApp::openRS('closed_XdocumentX', $event_args);
      $is_closed = $rs->Field('is_closed');
      if ($is_closed=='true')
        {
          WebApp::message(T_("It is closed and cannot be deleted!"));
        }
      else
        {
          WebApp::execDBCmd('del_XdocumentX', $event_args);
          WebApp::message(T_("Deleted."));
        }
    } 

  function onRender()
    {
      WebApp::setSVar("XdocumentsX_rs->recount", "true");

      $filter_condition = $this->get_filter_condition();
      WebApp::addVar("filter_condition", $filter_condition);
    }

  function get_filter_condition()
    {
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "XdocumentX_date", $date_filter);
      $XdocumentsX_filter = WebApp::getSVar("XdocumentXFilter->filter");

      $conditions[] = $date_filter;
      if ($XdocumentsX_filter!='')   $conditions[] = $XdocumentsX_filter;

      $filter_condition = implode(' AND ', $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
}
?>