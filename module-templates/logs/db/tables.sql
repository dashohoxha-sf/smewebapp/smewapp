
DROP TABLE IF EXISTS logs;
CREATE TABLE logs (
  id int(10) unsigned NOT NULL auto_increment,
  time datetime NOT NULL default '0000-00-00 00:00:00',
  event varchar(100) NOT NULL default '',
  details varchar(255) NOT NULL default '',
  PRIMARY KEY  (id)
) ENGINE=MyISAM COMMENT='The table that contains XlogsX.';
