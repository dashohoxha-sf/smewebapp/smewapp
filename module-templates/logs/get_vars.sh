
### get the values of the variables

if [ -f $destination/vars ]
then
  . $destination/vars
fi

if [ -z "$appname" ]
then
  read -p "appname[appname]=" appname
  appname=${appname:-appname}
fi

if [ -z "$package" ]
then
  read -p "package[package]=" package
  package=${package:-package}
fi

if [ -z "$module" ]
then
  read -p "module[logs]=" module
  module=${module:-logs}
fi

if [ -z "$log" ]
then
  read -p "log[log]=" log
  log=${log:-log}
fi

if [ -z "$logs" ]
then
  read -p "logs[logs]=" logs
  logs=${logs:-logs}
fi
