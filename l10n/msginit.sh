#!/bin/bash
### create initial translation files for a language

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### create an initial *.po file for the application
mkdir -p $lng/LC_MESSAGES/
msginit --input=smewapp.po --locale=$lng --no-translator \
        --output-file=$lng/LC_MESSAGES/smewapp.po
