#!/bin/bash
### update old *.po files with new messages extracted by xgettext.sh

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### convert the *.po file of the application
dir=$lng/LC_MESSAGES
mv $dir/smewapp.po smewapp-$lng-old.po
msgmerge --output-file=$dir/smewapp.po \
         --compendium=compendia/compendium-$lng.po   \
         smewapp-$lng-old.po smewapp.po
