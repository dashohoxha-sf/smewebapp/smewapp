#!/bin/bash

### go to this dir
cd $(dirname $0)

### get the translatable strings for the module templates
find ../.. -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --output=compendium.po

### get the translatable strings from the modules
find ../../modules/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --join-existing --output=compendium.po

### get the translatable strings from the php files in the app dir
xgettext -C --keyword=T_ --join-existing --output=compendium.po ../../*.php

### get the translatable strings from webobjects
find ../../webobjects/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --join-existing --output=compendium.po
