#!/bin/bash
### update all the modules

### go to this directory
cd $(dirname $0)

### package 'documents'
./update-module.sh documents documents purchases

### package 'admin'
./update-module.sh users admin users
./update-module.sh logs admin logs

### package 'inventory'
./update-module.sh items inventory warehouse

### package 'reports'
./update-module.sh summary_report reports purchases

### package 'tables'
./update-module.sh items_1 tables items
./update-module.sh tables tables simple_tables

