<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    admin
 * @subpackage logs
 */
class logFilter extends WebObject
{
  function init()
    {
      $time1 = time() - 24*60*60; //yesterday
      $time2 = time() + 24*60*60; //tomorrow
      $this->addSVars(array(
                            'time1' => date('Y-m-d H:i', $time1),
                            'time2' => date('Y-m-d H:i', $time2),
                            'event' => '',
                            'details' => ''
                            ));
    }

  function onParse()
    {
      WebApp::setSVar('logs->filter', $this->get_filter());
    }

  function get_filter()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      $conditions = array();

      //get the filter conditions
      $conditions[] = "(time >= '$time1' AND time <= '$time2')";
      if ($event!='')  $conditions[] = "event LIKE '%$event%'";
      if ($details!='')  $conditions[] = "details LIKE '%$details%'";

      $filter = '(' . implode(' AND ', $conditions) . ')';
      return $filter;
    }
}
?>