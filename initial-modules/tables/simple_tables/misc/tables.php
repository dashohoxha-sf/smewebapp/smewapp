<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package tables
 */
class tables extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'simple_tables/XitemsX.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_tables($event->args);
        }
    }

  function select_tables($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //simple_tables
        case 'tables/simple_tables/group1':
        case 'tables/simple_tables/group2':
        case 'tables/simple_tables/group3':
          $interface_title = T_("simple_tables");
          $module = 'simple_tables/simple_tables.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>