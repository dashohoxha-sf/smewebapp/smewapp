
DROP TABLE IF EXISTS table1;
CREATE TABLE table1 (
  product_id varchar(20) NOT NULL,
  item_id varchar(20) NOT NULL,
  unit varchar(10) default NULL,
  quantity int(11) default NULL,
  PRIMARY KEY  (product_id, item_id)
) TYPE=MyISAM
COMMENT='The table that contains some item_id for each product_id.';

DROP TABLE IF EXISTS table2;
CREATE TABLE table2 (
  product_id varchar(20) NOT NULL,
  item_id varchar(20) NOT NULL,
  unit varchar(10) default NULL,
  quantity int(11) default NULL,
  PRIMARY KEY  (product_id, item_id)
) TYPE=MyISAM
COMMENT='The table that contains some item_id for each product_id.';

DROP TABLE IF EXISTS table3;
CREATE TABLE table3 (
  product_id varchar(20) NOT NULL,
  item_id varchar(20) NOT NULL,
  unit varchar(10) default NULL,
  quantity int(11) default NULL,
  PRIMARY KEY  (product_id, item_id)
) TYPE=MyISAM
COMMENT='The table that contains some item_id for each product_id.';

