<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    tables
 * @subpackage items
 */
class itemEdit extends formWebObj
{
  var $item_data =  array( 
                          'item'   => '',
                          'unit'     => '',
                          'price'    => '',
                          'type'     => '',
                          'partners' => '',
                          'notes'    => ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new item in database */
  function on_add($event_args)
    {
      $this->item_data = array_merge($this->item_data, $event_args);

      //check that such an item does not exist in DB
      $item = $event_args['item'];
      $rs = WebApp::openRS('get_item', array('item'=>$item));
      if (!$rs->EOF())
        {
          $msg = T_("The item 'var_item' is already used for \n"
            . "somebody else.  Please choose another item.");
          $msg = str_replace('var_item', $item, $msg);
          WebApp::message($msg);
          $this->item_data['item'] = '';
          return;
        }

      //add the item
      WebApp::execDBCmd('add_item', $this->item_data);

      //set the new item as selected and change the mode to edit
      WebApp::setSVar('itemList->selected', $item);
      $this->setSVar('mode', 'edit');
    }

  /** delete the current item */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('delete_item');

      //the selected item is deleted,
      //select the first item in the list
      $itemList = WebApp::getObject('itemList');
      $itemList->select_first();

      //acknowledgment message
      WebApp::message(T_("Item deleted."));
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update item data
      WebApp::execDBCmd('update_item', $event_args);
    }

  function onParse()
    {
      //get the current item from the list of items
      $item = WebApp::getSVar('itemList->selected');

      if ($item==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $item_data = $this->item_data; 
        }
      else
        {
          $rs = WebApp::openRS('selected');
          $item_data = $rs->Fields();
        }
      WebApp::addVars($item_data);      
    }
}
?>