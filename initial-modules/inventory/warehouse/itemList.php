<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    inventory
 * @subpackage warehouse
 */

class itemList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('items_rs->current_page', $page);
    }
    
  function on_del($event_args)
    {
      WebApp::execDBCmd('del_item', $event_args);
    } 
    
  function on_add($event_args)
    {
      WebApp::setSVar('itemEdit->mode', 'add');
      WebApp::setSVar('itemEdit->item_id', UNDEFINED);
    } 

  function on_edit($event_args)
    {
      WebApp::setSVar('itemEdit->mode', 'edit');
      WebApp::setSVar('itemEdit->item_id', $event_args['item_id']);
    } 

  function onRender()
    {
      WebApp::setSVar('items_rs->recount', 'true');

      $filter_condition = WebApp::getSVar('itemFilter->filter_condition');
      WebApp::addVar('filter_condition', $filter_condition);
    }
}
?>


