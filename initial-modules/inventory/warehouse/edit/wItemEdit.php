<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    inventory
 * @subpackage warehouse
 */
include_once FORM_PATH.'formWebObj.php';

class wItemEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('wItem_id', UNDEFINED);
    }

  function on_save($event_args)
    {
      $record = $event_args;

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add the new wItem
          $this->insert_record($record, 'warehouse');

          //set 'wItems_rs->recount' to 'true'
          //so that the records are counted again
          WebApp::setSVar('wItems_rs->recount', 'true');
        }
      else if ($mode=='edit')
        {
          $record['wItem_id'] = $this->getSVar('wItem_id');
          $this->update_record($record, 'warehouse', 'wItem_id');
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
      $this->setSVar('wItem_id', UNDEFINED);
    }

  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
      $this->setSVar('wItem_id', UNDEFINED);
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $wItem = $this->pad_record(array(), 'warehouse');
          WebApp::addVars($wItem);
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_wItem');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
        }
    }
}
?>