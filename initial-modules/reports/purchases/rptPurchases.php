<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage purchases
 */

class rptPurchases extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('rptPurchaseItems_rs->current_page', $page);
    }
    
  function onRender()
    {
      WebApp::setSVar('rptPurchaseItems_rs->recount', 'true');
      $this->build_report_rs();
    }

  /** 
   * Build the recordset that will be displayed in the report
   * and add it to the webPage. Calculate also the {{total_value}}.
   */
  function build_report_rs()
    {
      //get the recordset that will be used (PagedRS or not)
      $rs_id = ( PRINT_MODE=='true' ?
                 'rptPurchaseItems_rs_print' :
                 'rptPurchaseItems_rs' );

      //get the items that will be displayed in the report
      $var_name = 'rptPurchaseFilter->filter_condition';
      $filter_condition = WebApp::getSVar($var_name);
      $params['filter_condition'] = $filter_condition;
      $items_rs = WebApp::openRS($rs_id, $params);

      //add some additional columns that will be displayed in report
      $items_rs->addCol('quantity', 0.0);
      $items_rs->addCol('value', 0.0);
      $items_rs->addCol('price', 0.0);
      $items_rs->addCol('partners', '');

      //get the Purchases that will be processed
      $arr_items = $items_rs->getColumn('item');
      $item_list = "'" . implode("', '", $arr_items) . "'";
      $params['item_list'] = $item_list;
      $Purchases_rs = WebApp::openRS('get_Purchases', $params);

      //sum up the quantity and the values of the Purchases for each item
      while (!$items_rs->EOF())
        {
          $item = $items_rs->Field('item');
          while ($Purchases_rs->Field('item') < $item)
            {
              $Purchases_rs->MoveNext();
            }
          while ($Purchases_rs->Field('item') == $item)
            {
              $quantity = $Purchases_rs->Field('quantity');
              $price = $Purchases_rs->Field('price');
              $partner = $Purchases_rs->Field('partner');

              $item_quantity = $items_rs->Field('quantity');
              $items_rs->setFld('quantity', $item_quantity + $quantity);
              $item_value = $items_rs->Field('value');
              $items_rs->setFld('value', $item_value + $quantity*$price);
              $item_partners = $items_rs->Field('partners');
              $items_rs->setFld('partners', $item_partners.$partner.' ');

              $Purchases_rs->MoveNext();
            }
          $items_rs->MoveNext();
        }

      //calculate the average price for each item,
      //process the partners list, get the total value, etc.
      $total_value = 0;
      $items_rs->MoveFirst();
      while (!$items_rs->EOF())
        {
          //calculate the average price
          $quantity = $items_rs->Field('quantity');
          $value = $items_rs->Field('value');
          $price = ($quantity==0 ? 0 : $value/$quantity);
          $price = round($price*100)/100;
          $items_rs->setFld('price', $price);

          //total value
          $total_value += $value;

          //process the partners
          $partners = $items_rs->Field('partners');
          $arr_p = explode(' ', trim($partners));
          sort($arr_p);
          $arr_partners = array();
          $last_p = '';
          for ($i=0; $i < sizeof($arr_p); $i++)
            {
              if ($arr_p[$i]==$last_p)  continue;
              $arr_partners[] = $arr_p[$i];
              $last_p = $arr_p[$i];
            }
          $items_rs->setFld('partners', implode(' ', $arr_partners));

          $items_rs->MoveNext();
        }

      WebApp::addVar('total_value', $total_value);

      global $webPage;
      $webPage->addRecordset($items_rs);
    }
}
?>


