<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    documents
 * @subpackage purchases
 */
class purchaseFilter extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             'field_1' => '',
                             'field_2' => '',
                             'field_3' => ''
                             ));
      $this->addSVar('filter', '');
    }

  function onParse()
    {
      $this->setSVar('filter', $this->get_filter());
    }

  function get_filter()
    {
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      $conditions = array();

      if ($field_1!='')
        $conditions[] = "field_1 LIKE '%$field_1%'";

      if ($field_2!='')
        {
          $ch = $field_2[0];
          if ($ch!='=' and $ch!='<' and $ch!='>') $field_2 = '='.$field_2;
          $conditions[] = "field_2 $field_2";
        }

      if ($field_3!='')
        $conditions[] = "field_3 LIKE '%$field_3%'";

      $filter = implode(' AND ', $conditions);
      if ($filter!='')  $filter = "($filter)";
      return $filter;
    }
}
?>