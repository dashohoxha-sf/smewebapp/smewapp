<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage purchases
 */
class purchaseEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('purchase_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_purchase');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $purchase = $this->pad_record(array(), 'purchases');
          WebApp::addVars($purchase);

          //title
          $title = T_("New purchase");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_purchase');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $purchase_id = $fields['purchase_id'];
          $title = T_("purchase").": $purchase_id";
          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['purchase_date']=='')
        $record['purchase_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_purchase($record);
        }
      else //mode=='edit'
        {
          $record['purchase_id'] = $this->getSVar('purchase_id');
          $this->update_record($record, 'purchases', 'purchase_id');
        }
    }

  /** create a new purchase with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_purchase');
      $record = $rs->Fields();
      unset($record['purchase_id']);
      $this->add_new_purchase($record);
    }

  function on_close($event_args)
    {
      $params['close_date'] = get_curr_date('Y-M-D');
      WebApp::execDBCmd('close_purchase', $params);
    }

  function add_new_purchase($purchase)
    {
      //add the new purchase
      $purchase['close_date'] = 'NULL';
      $this->insert_record($purchase, 'purchases');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'purchases_rs->recount' to 'true'
      //so that the records are counted again
      //and the new purchase is displayed at the end of the list
      WebApp::setSVar('purchases_rs->recount', 'true');

      //set the new purchase_id
      $rs = WebApp::openRS('get_last_purchase_id');
      $purchase_id = $rs->Field('last_purchase_id');
      $this->setSVar('purchase_id', $purchase_id);
    }
}
?>