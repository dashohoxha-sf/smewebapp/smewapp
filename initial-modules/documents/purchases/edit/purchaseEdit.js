// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function print_purchase()
{
  var interface_id = session.getVar('interface');
  var purchase_id = session.getVar('purchaseEdit->purchase_id');
  var args = 'purchase_id=' + purchase_id;
  print_template(interface_id, args);
}

function save()
{
  var form = document.purchaseEdit;
  if (!editable())      return;
  if (!validate(form))  return;
  
  var event_args = getEventArgs(form);
   
  var mode = session.getVar("purchaseEdit->mode");
  if (mode=="add")  saveFormData(form);
  SendEvent("purchaseEdit", "save", event_args);
}

/** create a new document with almost the same data */
function clone()
{
  var msg = T_("Creating a new document with the same content as this one.");
  if (confirm(msg))
    {
      SendEvent("purchaseEdit", "clone");
    }
}

function close_doc()
{
  var confirm_msg;
  confirm_msg = T_("Are you sure that you want to close it?");
  if (confirm(confirm_msg))
    {
      SendEvent('purchaseEdit', 'close');
    }
}

/** returns false if the purchase is closed */
function editable()
{
  var closed = session.getVar("purchaseEdit->closed");

  if (closed=="true")
    {
      alert(T_("It is closed and cannot be modified."));
      return false;
    }

  return true;
}

function validate(form)
{
  /*
  if (form.field_1.value=="")
    {
      alert(T_("Please don't leave empty field_1."));
      form.field_1.focus();
      return false;
    }
  */

  return true;
}

function date_not_in_future(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = arr_date[0];
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date > curr_date)
    {
      alert(T_("Warning: The selected date is later than the current date."));
      //set_current_date(txtbox);
    }
}

function date_not_in_past(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = (arr_date[0]*1) + 1;
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date < curr_date)
    {
      alert(T_("Warning: The selected date is earlier than the current date."));
      //set_current_date(txtbox);
    }
}

function set_current_date(txtbox)
{
  var curr_date = get_curr_date();
  var day = curr_date.getDate();
  var month = curr_date.getMonth() + 1;
  var year = curr_date.getFullYear();

  if (day < 10)    day   = "0" + day;
  if (month < 10)  month = "0" + month;
  str_date = day + '/' + month + '/' + year;
  txtbox.value = str_date;
}
