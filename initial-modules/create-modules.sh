#!/bin/bash
### create the initial modules

### go to this directory
cd $(dirname $0)

### package 'documents'
./init-module.sh documents documents purchases

### package 'admin'
./init-module.sh users admin users
./init-module.sh logs admin logs

### package 'inventory'
./init-module.sh items inventory warehouse

### package 'reports'
./init-module.sh summary_report reports purchases

### package 'tables'
./init-module.sh items_1 tables items
./init-module.sh tables tables simple_tables

