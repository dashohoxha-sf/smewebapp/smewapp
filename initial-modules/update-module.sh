#!/bin/bash
### update a module with the latest changes

if [[ $# -lt 3 ]]
then
  echo "Usage: $0 model package module"
  exit 1
fi

### go to this directory
cd $(dirname $0)

model=$1
package=$2
module=$3
path=$package/$module

### include application variables
. config.sh

### create the module
mkdir latest_module/
cp $path/vars latest_module/
../module-templates/$model/create_module.sh latest_module/

### import in the initial module
svn_load_dirs=/usr/share/doc/subversion-1.3.2/svn_load_dirs.pl
repos=file:///$svnroot/$appname/trunk/initial-modules/$package
$svn_load_dirs -wc $path $repos $module latest_module/

### clean up
rm -rf latest_module/

### commit changes in the initial module
svn commit $path -m ''

### merge the changes of the last commit to the trunk module
svn update $path
current=$(svn info $path | grep 'Revision: ' | cut -d' ' -f2)
let prev=current-1
svn merge $path@{$prev,$current} ../modules/$path

### commit the changes of the last module
echo
echo "Now resolve any conflicts in ../modules/$path and commit:"
echo "\$ svn commit ../modules/$path -m ''"
echo "Then update the DB tables as well:"
echo "\$ ../modules/$path/db/create.sh"
echo
