### This file contains some variables that are used
### by the scripts that handle modules (e.g. application
### name, database name, etc.) which are the same for
### all the modules.

appname=smewapp
svnroot=/data/svn-sf/smewebapp
dbhost=localhost
dbname=smewapp
dbuser=root
dbpasswd=
