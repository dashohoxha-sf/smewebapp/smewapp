#!/bin/bash
### create the initial module and import it in repository

### go to this directory
cd $(dirname $0)

if [[ $# -lt 3 ]]
then
  echo "Usage: $0 model package module"
  echo "where 'model' is the module-template that is used (e.g. documents)" 
  exit 1
fi

model=$1
package=$2
module=$3

### include application variables
. config.sh

### create the directories
svn_trunk=file://$svnroot/$appname/trunk
svn mkdir $svn_trunk/initial-modules -m ''  2> /dev/null
svn mkdir $svn_trunk/initial-modules/$package -m ''  2> /dev/null
svn update
svn mkdir $svn_trunk/modules -m ''  2> /dev/null
svn mkdir $svn_trunk/modules/$package -m ''  2> /dev/null
svn update ../modules

### prepare the directory of the module
dir=$package/$module
mkdir -p $dir
cat <<EOF > $dir/vars
appname=$appname
package=$package
module=$module
EOF

### create the module
../module-templates/$model/create_module.sh $dir

### make all scripts executable
find $dir/ -name '*.sh' | xargs chmod +x

### modify the DB variables
if [ -f $dir/db/vars ]
then
  cat <<EOF > $dir/db/vars
host=$dbhost
database=$dbname
user=$dbuser
passwd=$dbpasswd
EOF
fi

### import in svn the new module
svn add $dir
svn commit $dir -m 'initial import'

### copy the module in the trunk (main development branch)
svn copy $svn_trunk/{initial-modules,modules}/$dir -m ''

### update the working copy
svn update ../modules/

### create the db tables
../modules/$package/$module/db/create.sh
