<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items of the tabs. 
 *
 * @package     webobjects
 * @subpackage  tabs
 */
$menu_items = array(
                    "item1" => " Menu Item 1 ",
                    "item2" => " Menu Item 2 ",
                    "item3" => " Menu Item 3 ",
                    "etc1"  => " . . . ",
                    "etc2"  => " . . . "
                    );
?>
