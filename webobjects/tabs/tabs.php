<?php
/*
Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/


/**
 * @package     webobjects
 * @subpackage  tabs
 */
class tabs extends WebObject
{
  function init()
    {
      $items_file = $this->params["items"];
      $items_file = WebApp::replaceVars($items_file);
      $this->addSVar("items_file", $items_file);

      //set the first menu item as the selected item
      include $items_file; //read the menu items
      $first_item = key($menu_items);
      $this->addSVar("selected_item", $first_item); 
    }

  function on_select($event_args)
    {
      $item = $event_args["item"];
      $this->setSVar("selected_item", $item);
    } 

  function onRender()
    {
      $this->add_items_list();
    }

  function add_items_list()
    {
      $obj_vars = $this->getObjVars();
      $obj_name = $obj_vars["obj_name"];
      $obj_count = $obj_vars["obj_count"];
      $rs = new EditableRS($obj_name."_items");

      //read the menu items
      $items_file = $this->getSVar("items_file");
      include $items_file;

      //fill the recordset
      $selected = $this->getSVar("selected_item");
      while ( list($item, $label) = each($menu_items) )
        {
          $css_class = ($item==$selected ? "tabs-item-selected" : "tabs-item");
          $rec = array(
                       "item"  => $item, 
                       "label" => $label,
                       "class" => $css_class
                       );
          $rs->addRec($rec);
        }

      //set the recordset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>
