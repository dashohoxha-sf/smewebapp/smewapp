#!/bin/bash
### dump the DB tables of the module

### go to this directory
cd $(dirname $0)

### get the variables
. ../initial-modules/config.sh

### dump the database into the file dbname.sql
mysqldump --add-drop-table --allow-keyword \
          --complete-insert --extended-insert \
          --host=$dbhost --user=$dbuser --password=$dbpasswd \
          --databases $dbname > $dbname.sql

### dump the database structure into the file dbname-tables.sql
mysqldump --add-drop-table --allow-keyword --no-data \
          --host=$dbhost --user=$dbuser --password=$dbpasswd \
          --databases $dbname > $dbname-tables.sql

### create the database with the command:
### $ mysql -p -u $dbuser < $dbname.sql

