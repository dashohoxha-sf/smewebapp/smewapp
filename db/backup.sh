#!/bin/bash
### create a backup file and compress it (.gz)

if [ "$1" = "" ]
then
  echo "Usage: $0 fname"
  exit 1
fi

fname=$1

### go to this directory
cd $(dirname $0)

### get the variables
. ../initial-modules/config.sh

### backup the database into a file
mysqldump --add-drop-table --allow-keyword --complete-insert \
          --host=$dbhost --user=$dbuser --password=$dbpasswd \
          --databases $dbname > backup/$fname

### compress
rm -rf backup/$fname.gz
gzip backup/$fname

