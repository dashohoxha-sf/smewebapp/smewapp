#!/bin/bash
### create the DB tables of the application

### go to this directory
cd $(dirname $0)

### get the variables
. ../initial-modules/config.sh

### create the database 
echo "create database $dbname;" \
  | mysql --host=$dbhost --user=$dbuser --password=$dbpasswd

### restore the database table and initial data
mysql --host=$dbhost --user=$dbuser --password=$dbpasswd \
      --database=$dbname < smewapp.sql
