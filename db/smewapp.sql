-- MySQL dump 10.9
--
-- Host: localhost    Database: smewapp
-- ------------------------------------------------------
-- Server version	4.1.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `smewapp`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `smewapp` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `smewapp`;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `item` varchar(20) NOT NULL default '',
  `unit` varchar(10) NOT NULL default '',
  `price` smallint(6) default NULL,
  `type` varchar(20) default NULL,
  `partners` text,
  `notes` text,
  PRIMARY KEY  (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--


/*!40000 ALTER TABLE `items` DISABLE KEYS */;
LOCK TABLES `items` WRITE;
INSERT INTO `items` (`item`, `unit`, `price`, `type`, `partners`, `notes`) VALUES ('test1','u1',0,'','',''),('test2','',0,'','',''),('test3','',0,'','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(10) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `charset` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--


/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
LOCK TABLES `languages` WRITE;
INSERT INTO `languages` (`id`, `name`, `charset`) VALUES ('sq_AL','Albanian','iso-8859-1'),('en','English','iso-8859-1');
UNLOCK TABLES;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `event` varchar(100) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='The table that contains logs.';

--
-- Dumping data for table `logs`
--


/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
LOCK TABLES `logs` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `purchase_id` int(10) unsigned NOT NULL auto_increment,
  `purchase_date` date default '0000-00-00',
  `close_date` date default '0000-00-00',
  `timestamp` varchar(250) NOT NULL default '',
  `item` varchar(100) default '',
  `quantity` decimal(6,1) default '0.0',
  `price` decimal(6,1) default '0.0',
  `source` varchar(100) default '',
  `notes` text,
  PRIMARY KEY  (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--


/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
LOCK TABLES `purchases` WRITE;
INSERT INTO `purchases` (`purchase_id`, `purchase_date`, `close_date`, `timestamp`, `item`, `quantity`, `price`, `source`, `notes`) VALUES (2,'2006-02-21',NULL,'1140555879','test','4.0','10.0','s1','fdf'),(3,'2006-02-21','2006-02-21','1140555903','test1','4.0','10.0','s1','fdf');
UNLOCK TABLES;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--


/*!40000 ALTER TABLE `session` DISABLE KEYS */;
LOCK TABLES `session` WRITE;
INSERT INTO `session` (`id`, `vars`) VALUES ('IP: 192.168.1.1; DATE: 2006-06-21 14:06:13','a:4:{s:8:\"username\";s:5:\"guest\";s:8:\"password\";s:13:\"1777sjjCbXjlM\";s:4:\"u_id\";s:2:\"12\";s:13:\"access_rights\";s:260:\"documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,backup,user_profile\";}'),('IP: 192.168.1.1; DATE: 2006-06-21 16:10:58','a:4:{s:8:\"username\";s:5:\"guest\";s:8:\"password\";s:13:\"1777sjjCbXjlM\";s:4:\"u_id\";s:2:\"12\";s:13:\"access_rights\";s:260:\"documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,backup,user_profile\";}'),('IP: 192.168.1.1; DATE: 2006-06-21 16:11:06','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$ct1swZ3B$mMegeDv0hE.IeKfLxrMou.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 192.168.1.1; DATE: 2006-06-21 16:16:31','a:4:{s:8:\"username\";s:5:\"guest\";s:8:\"password\";s:13:\"1777sjjCbXjlM\";s:4:\"u_id\";s:2:\"12\";s:13:\"access_rights\";s:260:\"documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,backup,user_profile\";}'),('IP: 192.168.1.1; DATE: 2006-06-21 16:17:19','a:4:{s:8:\"username\";s:5:\"guest\";s:8:\"password\";s:13:\"1777sjjCbXjlM\";s:4:\"u_id\";s:2:\"12\";s:13:\"access_rights\";s:271:\"documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,admin/logs,backup,user_profile\";}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(20) default NULL,
  `password` varchar(20) NOT NULL default '',
  `firstname` varchar(20) NOT NULL default '',
  `lastname` varchar(20) default NULL,
  `e_mail` varchar(25) default NULL,
  `phone1` varchar(20) default NULL,
  `phone2` varchar(20) default NULL,
  `address` varchar(35) default NULL,
  `notes` text,
  `salary` smallint(6) default NULL,
  `title` varchar(50) default NULL,
  `access_rights` text,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `e_mail`, `phone1`, `phone2`, `address`, `notes`, `salary`, `title`, `access_rights`) VALUES (15,'dasho','23IRRYx9KhegU','Dashamir','Hoxha','dashohoxha@users.sf.net','','','','',0,'','documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,admin/logs,backup,user_profile'),(17,'test1','23USZiR8kBoEQ','','','','','','','',0,'','documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,backup,user_profile'),(12,'guest','1777sjjCbXjlM','Guest','Guest','','','','','',0,'{title}','documents/purchases/list,documents/sellings/list,documents/productions/list,inventory/warehouse,inventory/repository,reports/purchases,reports/sellings,reports/productions,reports/balance,tables/items,tables/simple_tables/group1,admin/users,admin/logs,backup,user_profile'),(18,'test2','26JF0uvfmNbKE','','','','','','','',0,'',NULL),(19,'test3','','','','','','','','',0,'',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `wItem_id` int(11) NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `unit` varchar(10) default NULL,
  `quantity` int(11) default NULL,
  `price` int(11) default NULL,
  `value` int(11) default NULL,
  PRIMARY KEY  (`wItem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='The table that contains wItems.';

--
-- Dumping data for table `warehouse`
--


/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
LOCK TABLES `warehouse` WRITE;
INSERT INTO `warehouse` (`wItem_id`, `name`, `unit`, `quantity`, `price`, `value`) VALUES (1,'Test','u1',0,10,100);
UNLOCK TABLES;
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

