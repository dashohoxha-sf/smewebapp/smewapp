#!/bin/bash

### set application name
appname=smewapp

### go to this directory
cd $(dirname $0)

### create directory download
rm -rf download/
mkdir download

### generate HTML and PDF documentation using phpDocumentor
rm -rf phpdocu/
./phpdoc_html.sh
./phpdoc_pdf.sh

### generate HTML and PDF documentation using doxygen
rm -rf doxygen
/usr/local/bin/doxygen doxygen.cfg
cd doxygen/latex/
pdflatex refman
cd ../..

### create phpDocumentor downloadable files
rm -rf $appname-phpdocu
mv phpdocu $appname-phpdocu
tar cfz download/$appname-phpdocu.tar.gz $appname-phpdocu/
cp download/documentation.pdf download/$appname-phpdocu.pdf
gzip download/$appname-phpdocu.pdf
mv download/documentation.pdf download/$appname-phpdocu.pdf

### create doxygen downloadable files
rm -rf $appname-doxygen
mv doxygen $appname-doxygen
tar cfz download/$appname-doxygen.tar.gz $appname-doxygen/html/
cp $appname-doxygen/latex/refman.pdf download/$appname-doxygen.pdf
gzip download/$appname-doxygen.pdf
mv $appname-doxygen/latex/refman.pdf download/$appname-doxygen.pdf
