<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Called at the login time.
 * @package login
 */

if (!valid_user())
{	
  //the user is not authenticated, go to the login page
  $event->targetPage = 'login/login.html';
  WebApp::addGlobalVar('login_error', 'true');
  WebApp::message(T_("Wrong username or password!"));
}
else
{
  // add some state variables and create the user menus
  init_user();
  include_once MENU.'func.create_user_menu.php';
  $first_interface = create_user_menu();

  //open the main page
  $event->targetPage = 'main/main.html';
  $event->target = 'main';
  $event->name = 'select';
  $event->args['interface'] = $first_interface;
}

/** 
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  global $event;
  $username = $event->args['username'];
  $password = $event->args['password'];

  if ($username=='superuser')
    {
      $passwd = '$1$ct1swZ3B$mMegeDv0hE.IeKfLxrMou.';
      $rs = new EditableRS('user_data');
      $rs->addRec(array('password'=>$passwd));
    }
  else
    {
      $query = "SELECT * FROM users WHERE username='$username'";
      $rs = new EditableRS('user_data', $query);
      $rs->Open();
    }
  global $webPage;
  $webPage->addRecordset($rs);

  if ($rs->EOF())  return false; //query returned no records

  $crypted_passwd = $rs->Field('password');
  $valid = ($crypted_passwd == crypt($password, $crypted_passwd));

  return $valid;
}

/**
 * Add some state variables and create the user menus.
 */
function init_user()
{
  global $event;
  $username = $event->args['username'];
  $rs = WebApp::openRS('user_data');

  WebApp::addSVar('username', $username, 'DB');
  WebApp::addSVar('password', $rs->Field('password'), 'DB');

  if ($username=='superuser')
    {
      WebApp::addSVar('u_id', '0', 'DB');
    }
  else
    {
      //save some data in the session 
      WebApp::addSVar('u_id', $rs->Field('user_id'), 'DB');
      WebApp::addSVar('access_rights', $rs->Field('access_rights'), 'DB');
      WebApp::addSVar('firstname', $rs->Field('firstname'));
      WebApp::addSVar('lastname', $rs->Field('lastname'));
    }
}
?>