// -*-C-*- //tell emacs to use the C mode

var MENU_POS = [
    {
      // item sizes
      'width': 80,
      'height': 19,
      // menu block offset from the origin:
      //  for root level origin is upper left corner of the page
      //  for other levels origin is upper left corner of parent item
      'block_top': 7,
      'block_left': 8,
      // offsets between items of the same level
      'top': 0,
      'left': 82,
      // time in milliseconds before menu is hidden after cursor has gone out
      // of any items
      'hide_delay': 200,
      'css' : {
        'inner' : 'minner',
        'outer' : ['moout', 'moover', 'modown']
      }
    },
    {
      'width': 120,
      'top': 21,
      'left': 0,
      'block_top': 21,
      'block_left': 0
    },
    {
      'block_top': 5,
      'block_left': 115
    }
];

