// -*-C-*- 

var MENU_ITEMS =
[
  ['Veprimet', null, null,
    ['Blerjet', 'javascript:select(\'blerjet\')', null],
    ['Prodhimet', 'javascript:select(\'prodhimet\')', null],
    ['Shitjet', 'javascript:select(\'shitjet\')', null],
    ['Pagesat', 'javascript:select(\'pagesat\')', null],
   ],
  ['Raportet', null, null,
    ['Magazina', 'javascript:select(\'magazina\')', null],
    ['Frigoriferi', 'javascript:select(\'frigoriferi\')', null],
    ['Koshi', 'javascript:select(\'koshi\')', null],
   ],
  ['Tabelat', null, null,
    ['Lenda e Pare', 'javascript:select(\'lenda_e_pare\')', null],
    ['Produktet', 'javascript:select(\'produktet\')', null],
    ['Partneret', 'javascript:select(\'partneret\')', null],
    ['Shpenzime', 'javascript:select(\'shpenzime\')', null],
   ],
  ['Admin', null, null,
    ['Users', 'javascript:select(\'users\')', null],
    ['Staff', 'javascript:select(\'staff\')', null],
    ['Backup/Restore', 'javascript:select(\'backup\')', null],
   ],
 ];
