<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package menu
 */
class MenuItem
{
  var $id;       //identifies this menu element
  var $caption;  //text that will be shown in the menu
  var $link;     //link associated with this menu item
  var $parent;   //parent of this element in the hierarchy of the menu
  var $children; //array of subelements

  function MenuItem($id =UNDEFINED, $caption ='menu item', $link =UNDEFINED)
    {
      $this->id = $id ;
      $this->caption = $caption;
      if ($link=='')  $link=UNDEFINED;
      $this->link = $link;
      $this->parent = UNDEFINED;
      $this->children = array();
    }

  function add_child(&$child)
    {
      $this->children[] = $child;
      $child->parent = &$this;
    }
        
  function nr_children()
    {
      return sizeof($this->children);
    }

  function has_children()
    {
      $nr_ch = $this->nr_children();
      return ($nr_ch <> 0);
    }

  /**
   * ToDo: fix it, it doesn't seem to work well
   */
  function level()
    {
      $level = 0;
      $item = &$this;
      while ($item->parent<>UNDEFINED);
        {
          $item = &$item->parent;
          $level++;
        }

      return $level;
    }

  /////////// output functions /////////////
        
  /** returns the menu element as an xml string indented by $indent */
  function to_xml($indent)
    {
      $xml_menu = $indent.'<item id="'.$this->id.'"'
        . ' caption="'.$this->caption.'"';
      if ($this->link<>UNDEFINED)
        {
          $xml_menu .= ' link="'.$this->link.'"';
        }

      if (!$this->has_children())
        {
          $xml_menu .= " />\n";
        }
      else
        {
          $xml_menu .= ">\n";
          $xml_menu .= $this->children_to_xml("  ".$indent);
          $xml_menu .= $indent."</item>\n";
        }
      return $xml_menu;
    }

  /** returns the menu element as an xml string indented by $indent */
  function children_to_xml($indent)
    {
      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $child = $this->children[$i];
          $xml_menu .= $child->to_xml($indent);
        }
      return $xml_menu;
    }

  /**
   * returns the element and all its subelements
   * in an easily readable text format
   */
  function to_text($indent)                //debug
    {
      $txt_menu = $indent . "> '".$this->id."' -> '".$this->caption."'";
      if ($this->link<>UNDEFINED)
        {
          $txt_menu .= " -> '".$this->link."'";
        }
      $txt_menu .= "\n";

      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $child = $this->children[$i];
          $txt_menu .= $child->to_text("    ".$indent);
        }

      return $txt_menu;
    }

  /**
   * Returns a JS array containing the items of the menu
   * in the format required by TigraMenu.
   */
  function to_js_arr($indent)
    {
      $caption = "'$this->caption'";
      $link = $this->link;
      if ($link==UNDEFINED)  $link = "javascript:select(\'$this->id\')";
      if ($link<>'null')     $link = "'$link'";

      $js_arr = $indent."[$caption, $link, null";

      if (!$this->has_children())
        {
          $js_arr .= "],\n";
        }
      else
        {
          $js_arr .= ",\n";
          for ($i=0; $i < $this->nr_children(); $i++)
            {
              $child = $this->children[$i];
              $js_arr .= $child->to_js_arr('  '.$indent);
            }
          $js_arr .= $indent." ],\n";
        }

      return $js_arr;
    }
}
?>