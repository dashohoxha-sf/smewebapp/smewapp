<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__)."/class.MenuItem.php";

/**
 * @package menu
 */ 
class XMLMenu
{
  var $root;             //root element of the tree
  var $stack;            //stack of elements in processing order
  var $currentElement;

  function XMLMenu($file_name)
    {
      $this->stack = array();
      $this->currentElement = new MenuItem;
      $this->parse($file_name);
      $this->root = $this->currentElement->children[0];
    }
        
  function getMenu()
    {
      return $this->root;
    }

  function parse($file_name)
    {
      $xml_parser = xml_parser_create();

      xml_set_object($xml_parser, $this);  //this works only on PHP4 or later
      xml_set_element_handler($xml_parser, "startElementHandler", "endElementHandler");
      xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 0);

      if (!($fp = fopen($file_name, "r")))
        {
          die("could not open XML input");
        }
      while ($data = fread($fp, 4096))
        {
          if (!xml_parse($xml_parser, $data, feof($fp)))
            {
              die(sprintf("XML error: %s at line %d",
                          xml_error_string(xml_get_error_code($xml_parser)),
                          xml_get_current_line_number($xml_parser)));
            }
        }
      xml_parser_free($xml_parser);
    }

  function startElementHandler($parser, $name, $attrs)
    {
      extract($attrs);

      switch ($name)
        {
        case "menu":
          $item = new MenuRoot($id);
          break;
        case "item":
          $item = new MenuItem($id, $caption, $link);
          break;
        }
                
      array_push($this->stack, $this->currentElement);
      $this->currentElement = &$item;
    }

  function endElementHandler($parser, $name)
    {
      $item = $this->currentElement;
      $this->currentElement = array_pop($this->stack);
      $this->currentElement->add_child($item);
    }
}
?>