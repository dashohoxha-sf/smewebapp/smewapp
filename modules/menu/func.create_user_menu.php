<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Creates the JS menu according to the access rights
 * of the user and save it in a cache file. Returns
 * the first interface that will be displayed to the user.
 */
function create_user_menu()
{
  include_once MENU.'class.MenuRoot.php';
  include_once MENU.'class.XMLMenu.php';

  //parse menu from XML file and create the menu tree structure
  $lng = WebApp::getSVar('language');
  $fname = MENU."menu_$lng.xml";
  if (!file_exists($fname))  $fname = MENU."menu_en.xml";
  $xmlMenu = new XMLMenu($fname);
  $menu = $xmlMenu->getMenu();
  
  $u_id = WebApp::getSVar('u_id');
  if ($u_id==UNDEFINED)  return;

  if ($u_id==0) //superuser
    {
      //add one more item for the superuser
      $item = new MenuItem('superuser', 'Superuser');
      $nr = $menu->nr_children();
      $menu->children[$nr-1]->add_child($item);

      $first_interface = 'superuser';
    }
  else
    {
      //filter the menu according to user access rights 
      $access_rights = WebApp::getSVar('access_rights');
      $menu = $menu->filter($access_rights);
      $menu = $menu->compact();

      //get the first interface
      $item = &$menu;
      while (($item->link=='null') and ($item->nr_children() > 0))
        {
          $item = &$item->children[0];
        }
      if ($item->link<>'null')  $first_interface = $item->id;
      else
        {
          $first_interface = 'user_profile';
          $msg = T_("You have no rights in the application,\n except modifying your profile.");
          WebApp::message($msg);
        }
    }

  //write menu to a cache file, according to the user id
  $menu->write_js(MENU."/cache/menu_items_$u_id.js");

  return $first_interface;
}
?>