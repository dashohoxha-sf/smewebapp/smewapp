// -*-C-*-
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function edit(menu_item)
{
  SendEvent('edit_menu', 'select', 'item_id='+menu_item);
}

function update()
{
  var form = document.edit_menu_item;
  var new_id = form.item_id.value;
  var new_caption = form.item_caption.value;

  if (new_id=='')
    {
      alert('ID cannot be empty.');
      form.item_id.focus();
      return false;
    }
  if (new_caption=='')
    {
      alert('Caption cannot be empty.');
      form.item_caption.focus();
      return false;
    }

  var event_args = 'new_id='+new_id+';new_caption='+new_caption;
  SendEvent('edit_menu', 'update', event_args);
}

function del()
{
  var msg = 'You are deleting this item and all its subitems.';
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'delete');
    }
}

function move_up(item_id)
{
  SendEvent('edit_menu', 'move_up', 'item_id='+item_id);
}

function move_down(item_id)
{
  SendEvent('edit_menu', 'move_down', 'item_id='+item_id);
}

function add_subitem()
{
  var form = document.add_new_subitem;
  var new_id = form.new_id.value;
  var new_caption = form.new_caption.value;

  if (new_id=='')
    {
      alert('Please fill the ID field.');
      form.new_id.focus();
      return false;
    }
  if (new_caption=='')
    {
      alert('Please fill the Caption field.');
      form.new_caption.focus();
      return false;
    }

  var event_args = 'new_id='+new_id+';new_caption='+new_caption;
  SendEvent('edit_menu', 'add_subitem', event_args);
}

function apply()
{
  var msg = 'This will copy the modified menu to the main menu.';
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'apply');
    }
}

function cancel()
{
  var msg = 'This will get a fresh copy of the main menu,'
    +' discarding any modifications';
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'cancel');
    }
}
