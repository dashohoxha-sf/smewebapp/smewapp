<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package menu
 */
class edit_menu extends WebObject
{
  function init()
    {
      //the menu item that is being edited, initially the root
      $this->addSVar('item_id', 'menu');
    }

  /** select an item for editing */
  function on_select($event_args)
    {
      $item_id = $event_args['item_id'];
      $this->setSVar('item_id', $item_id);
    }

  function transform_menu($transformer, $arr_params =array())
    {
      //apply the transformer and get the new menu
      $new_menu = $this->transform($transformer, $arr_params);

      //save the transformed menu
      $menu_xml = MENU.'edit/menu.xml';
      write_file($menu_xml, $new_menu);
    }

  /**
   * Applies the given xsl transformer to menu.xml and returns 
   * the result. $arr_params is an associative array of parameters. 
   */
  function transform($transformer, $arr_params =array())
    {
      //construct the string $params
      $params = '';
      while (list($p_name, $p_value) = each($arr_params))
        {
          $params .= "--stringparam $p_name \"$p_value\" ";
        }

      //apply the $transformer with $params to menu.xml
      $menu_xml = MENU.'edit/menu.xml';
      $xsl_file = MENU."edit/xsl/$transformer";
      $result = shell("xsltproc $params $xsl_file $menu_xml");
      //print "<xmp>$result</xmp>\n";

      return $result;
    }

  /** save modifications in id and caption of the current item */
  function on_update($event_args)
    {
      //if the id has changed, check that the new id does not exist
      $item_id = $this->getSVar('item_id');
      $new_id = $event_args['new_id'];
      $output = $this->transform('get_id.xsl', array('id'=>$new_id));
      if ($new_id <> $item_id and $output==$new_id)
        {
          WebApp::message("Another menu item with id '$new_id' already exists.");
          return;
        }

      $params = $event_args;
      $params['id'] = $item_id;
      $this->transform_menu('update.xsl', $params);
      $this->setSVar('item_id', $new_id);
    }

  /** delete the current item and set the parent item as the current one */
  function on_delete($event_args)
    {
      $params['id'] = $this->getSVar('item_id');
      $parent_id = $this->transform('get_parent_id.xsl', $params);
      $this->transform_menu('delete.xsl', $params);
      $this->setSVar('item_id', $parent_id);
    }

  /** move the current item up */
  function on_move_up($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_up.xsl', $params);
    }

  /** move the current item down */
  function on_move_down($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_down.xsl', $params);
    }

  /** add a new subitem to the current item */
  function on_add_subitem($event_args)
    {
      //check that the new id does not exist
      $new_id = $event_args['new_id'];
      $output = $this->transform('get_id.xsl', array('id'=>$new_id));
      if ($output==$new_id)
        {
          WebApp::message("Another menu item with id '$new_id' already exists.");
          return;
        }

      $params = $event_args;
      $params['id'] = $this->getSVar('item_id');
      $this->transform_menu('add_subitem.xsl', $params);
    }

  /** apply the modifications to the main menu */
  function on_apply($event_args)
    {
      //update menu.xml
      $menu_xml = MENU.'menu.xml';
      $edit_menu_xml = MENU.'edit/menu.xml';
      shell("cp $edit_menu_xml $menu_xml");

      //update menu_items.js
      $menu_items = $this->transform('menu_items.xsl');
      write_file(MENU.'menu_items.js', $menu_items);
    }

  /** discard any modifications and get a copy of the main menu */
  function on_cancel($event_args)
    {
      $menu_xml = MENU.'menu.xml';
      $edit_menu_xml = MENU.'edit/menu.xml';
      shell("cp $menu_xml $edit_menu_xml");
    }

  function onRender()
    {
      //this variable makes visible the edit menu in menu.html
      WebApp::addGlobalVar('edit_menu_visible', 'true');

      $item_id = $this->getSVar('item_id');

      //get the selected item and subitems
      $items = $this->transform('subitems.xsl', array('id'=>$item_id));
      $arr_lines = explode("\n", $items);

      //get the caption of the selected item (from the first line)
      list($id, $item_caption) = split(' ', $arr_lines[0], 2);
      WebApp::addVar('item_caption', $item_caption);

      //create a recordset with id-s and captions of the subitems
      $rs = new EditableRS('subitems');
      for ($i=1; $i < sizeof($arr_lines); $i++)
        {
          list($id, $caption) = split(' ', $arr_lines[$i], 2);
          if ($id=='')  continue;
          $rs->addRec(compact('id','caption'));
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>