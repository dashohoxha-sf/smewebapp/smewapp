<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (c) 2004 Dashamir Hoxha, dashohoxha@users.sourceforge.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
This transformation appends a new child item to the selected item.
It is called with parameters 'id', 'new_id', 'new_caption', like this: 
xsltproc -stringparam id $id \
         -stringparam new_id $new_id \
         -stringparam new_caption $new_caption \
         add_subitem.xsl menu.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />

<xsl:template match="menu[@id=$id]">
  <menu id="{@id}">
    <xsl:apply-templates />
    <item id="{$new_id}" caption="{$new_caption}" />
  </menu>
</xsl:template>

<xsl:template match="item[@id=$id]">
  <item id="{@id}" caption="{@caption}" link="null">
    <xsl:apply-templates />
    <item id="{$new_id}" caption="{$new_caption}" />
  </item>
</xsl:template>

<!-- copy everything else -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*" />
    <xsl:apply-templates select="node()" />
  </xsl:copy>
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
