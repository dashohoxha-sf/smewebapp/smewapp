// -*-C-*- //tell emacs to use the C mode

var MENU_ITEMS = 
[
  ['Records', null, null,
    ['Purchases', 'javascript:book(\'documents/purchases/list\')', null],
    ['Sellings', 'javascript:book(\'documents/sellings/list\')', null],
    ['Productions', 'javascript:book(\'documents/productions/list\')', null],
   ],
  ['States', null, null,
    ['Warehouse', 'javascript:book(\'inventory/warehouse\')', null],
    ['Repository', 'javascript:book(\'inventory/repository\')', null],
   ],
  ['Reports', null, null,
    ['Purchases', 'javascript:book(\'reports/purchases\')', null],
    ['Sellings', 'javascript:book(\'reports/sellings\')', null],
    ['Productions', 'javascript:book(\'reports/productions\')', null],
    ['Balance', 'javascript:book(\'reports/balance\')', null],
   ],
  ['Tables', null, null,
    ['Items', 'javascript:book(\'tables/items\')', null],
    ['Tables', 'javascript:book(\'tables/simple_tables/group1\')', null],
   ],
  ['Admin', null, null,
    ['Users', 'javascript:book(\'admin/users\')', null],
    ['Logs', 'javascript:book(\'admin/logs\')', null],
    ['Backup/Restore', 'javascript:book(\'backup\')', null],
   ],
];
