<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage purchases
 */
class rptPurchaseFilter extends WebObject
{
  function init()
    {
      $this->addSVar('item', '');
      $this->addSVar('filter_condition', '(1=1)');
    }

  function afterParse()
    {
      $this->setSVar('filter_condition', $this->get_filter_condition());
    }

  function get_filter_condition()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      //get the date filter
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "Purchase_date", $date_filter);
      $conditions[] = $date_filter;

      if ($item!='')
        $conditions[] = "item LIKE '%$item%'";

      $filter = '(' . implode(' AND ',$conditions) . ')';
      return $filter;
    }
}
?>