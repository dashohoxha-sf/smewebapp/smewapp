<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 * @subpackage superuser
 */
class superuser extends WebObject
{
  function onParse()
    {
      $module = WebApp::getSVar("tabs3::superuser->selected_item");
      switch ($module)
        {
        case "change_password":
          $module_file = "change_password/change_password.html";
          break;
        case "edit_tables":
          $module_file = "edit_tables/edit_tables.html";
          break;
        case "init_db":
          $module_file = "init_db/init_db.html";
          break;
        case "edit_menu":
          $module_file = "edit_menu/edit_menu.html";
          break;
        }
      WebApp::addVar("module_file", $module_file);
    }
}
?>