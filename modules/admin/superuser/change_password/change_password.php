<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 * @subpackage superuser
 */
class change_password extends WebObject
{
  function on_change($event_args)
    {
      //check that the old password supplied is correct
      $rs = WebApp::openRS("getPassword");
      $crypted_passwd = $rs->Field("password");
      $old_passwd = $event_args["old_password"];
      if ($crypted_passwd == crypt($old_passwd, $crypted_passwd))
        {
          //encrypt the new password and save it
          $new_password = $event_args["new_password"];
          srand(time());
          $password = crypt($new_password, rand());
          $query_vars["password"] = $password;
          WebApp::execDBCmd("savePassword", $query_vars);
          WebApp::message("Password changed successfully.");
        }
      else
        {
          $msg = "The old password you supplied is not correct!\n"
            ."Password not changed. Please try again.";
          WebApp::message($msg);
        }         
    }
}
?>