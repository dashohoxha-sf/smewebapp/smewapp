// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function change_passwd()
{
  var event_args;
  var form = document.change_password;
  var old_password = form.old_password.value;
  var new_password_1 = form.new_password_1.value;
  var new_password_2 = form.new_password_2.value;

  //check that New Password and Confirm Password are equal
  if (new_password_1 != new_password_2)
    {
      alert('New passwords are not equal! Please try again.');
      form.new_password_1.value = '';
      form.new_password_2.value = '';
      return;
    }

  event_args = 'new_password=' + new_password_1 
    + ';' + 'old_password=' + old_password;
  SendEvent('change_password', 'change', event_args);
}
