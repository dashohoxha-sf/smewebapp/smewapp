<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 */
class admin extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'user_profile/user_profile.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_interface($event->args);
        }
    }

  function select_interface($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
        default:
        case 'user_profile':
          $interface_title = T_("User Profile");
          $module  = 'user_profile/user_profile.html';
          break;

        case 'superuser':
          $interface_title = T_("Superuser");
          $module  = 'superuser/superuser.html';
          break;

        case 'change_date':
          if (TEST)
            {
              $interface_title = T_("Change the Date");
              $module = 'change_date/change_date.html';
            }
          break;

          //users
        case 'admin/users':
          $interface_title = T_("Users");
          $module = 'users/users.html';
          break;

          //logs
        case 'admin/logs':
          $interface_title = T_("logs");
          $module = 'logs/logs.html';
          break;

        case 'backup':
          $interface_title = T_("Backup/Restore");
          $module = 'backup/backup.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>