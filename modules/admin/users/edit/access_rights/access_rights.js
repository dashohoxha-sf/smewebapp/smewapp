// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function save_acc_rights()
{
  var access_rights = getCheckedMenuItems();
  var event_args = 'access_rights='+access_rights;
  SendEvent('access_rights', 'save_acc_rights', event_args);  
}

function getCheckedMenuItems()
{
  var access_rights = document.form_access_rights.access_rights;
  var checked_items = new Array;
  for(i=0; access_rights[i]; i++)
    {
      if (access_rights[i].checked)
        {
          checked_items.push(access_rights[i].value);
        }
    }

  return checked_items.join();
}
