<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once MENU."class.MenuRoot.php";
include_once MENU."class.XMLMenu.php";

/**
 * @package    admin
 * @subpackage users
 */
class access_rights extends WebObject
{
  /** array that contains the access rights of the current user */
  var $access_rights = array();

  function on_save_acc_rights($event_args)
    {
      //modify access rights
      $interfaces = $event_args['access_rights'];
      $event_args['access_rights'] = $this->modify_access_rights($interfaces);

      //update user data
      WebApp::execDBCmd('update_access_rights', $event_args);
    }

  /**
   * According to the selected interfaces, adds some more interfaces.
   */
  function modify_access_rights($interfaces)
    {
      $arr_interfaces = explode(',', $interfaces);
      for ($i=0; $i < sizeof($arr_interfaces); $i++)
        {
          $interface = $arr_interfaces[$i];
          /*
          switch ($interface)
            {
            case 'buletinet/offseti/new':
              $interfaces .= ',buletinet/offseti/edit';
              break;
            case 'buletinet/parapreg/new':
              $interfaces .= ',buletinet/parapreg/edit';
              break;
            case 'buletinet/lidhja/new':
              $interfaces .= ',buletinet/lidhja/edit';
              break;
            }
          */
        }
      $interfaces .= ',user_profile';

      return $interfaces;
    }

  function onRender()
    {
      //get the access rights of the user
      $rs = WebApp::openRS('get_user_access_rights');
      $access_rights = $rs->Field('access_rights');
      $this->access_rights = explode(',', $access_rights);

      //build the html code of the menu items
      $xmlMenu = new XMLMenu(MENU."menu.xml");
      $menu = $xmlMenu->getMenu();
      $html_menu = $this->menu_to_html($menu);

      WebApp::addVar("HTML_MENU", $html_menu);
    }

  function menu_to_html($menu)
    {
      $html = "
<table class='table_accr' cellspacing='5' cellpadding='0'>
  <tr>
";

      for ($i=0; $i < $menu->nr_children(); $i++)
        {
          if ($i % 3 == 0)
            {
              $html .= "  </tr>\n";
              $html .= "  <tr>\n";
            }

          $html .= "    <td valign='top' class='menu'>\n";
          $html .= "      <dl>\n";
          $item = $menu->children[$i];
          $html .= $this->menuitem_to_html($item, '        ');
          $html .= "      </dl>\n";
          $html .= "    </td>\n";
        }

      $html .= "
  </tr>
</table>
";

      return $html;
    }

  function menuitem_to_html($item, $indent)
    {
      if ($item->link=='null')
        {
          $html .= $indent."<dt class='menutitle'>\n";
          $html .= $indent."  ".$item->caption."\n";
          $html .= $indent."</dt>\n";
        }
      else
        {
          $id = $item->id;
          $checked = (in_array($id, $this->access_rights) ? 'checked' : '');
          $html .= $indent."<dt class='menuitem'>\n";
          $html .= $indent."  <input type='checkbox' name='access_rights'"
            . " value='$id' $checked>\n";
          $html .= $indent."  ".$item->caption."\n";
          $html .= $indent."</dt>\n";
        }

      if ($item->has_children())
        {
          $html .= $indent."<dd>\n";
          $html .= $indent."  <dl>\n";
          for ($i=0; $i < $item->nr_children(); $i++)
            {
              $subitem = $item->children[$i];
              $html .= $this->menuitem_to_html($subitem, '    '.$indent);
            }
          $html .= $indent."  </dl>\n";
          $html .= $indent."</dd>\n";
        }

      return $html;
    }
}
?>