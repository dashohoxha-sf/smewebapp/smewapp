// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function save()
{
  var event_args;
  var form = document.user_profile;
  var firstname = form.firstname.value;
  var lastname = form.lastname.value;
  var old_password = form.old_password.value;
  var new_password_1 = form.new_password_1.value;
  var new_password_2 = form.new_password_2.value;
  var phone1 = form.phone1.value;
  var phone2 = form.phone2.value;
  var e_mail = form.e_mail.value;
  var address = form.address.value;
  var args = new Array();

  //check that New Password and Confirm Password are equal
  if (new_password_1!=new_password_2)
    {
      alert('Passwords do not match. Please try again.');
      form.new_password_1.value = '';
      form.new_password_2.value = '';
      form.new_password_1.focus();
      return;
    }

  args.push('firstname=' + firstname);
  args.push('lastname=' + lastname);
  args.push('new_password=' + new_password_1);
  args.push('old_password=' + old_password);
  args.push('phone1=' + phone1);
  args.push('phone2=' + phone2);
  args.push('e_mail=' + e_mail);
  args.push('address=' + encode_arg_value(address));

  event_args = args.join(';');
  SendEvent('user_profile', 'save', event_args);
}
