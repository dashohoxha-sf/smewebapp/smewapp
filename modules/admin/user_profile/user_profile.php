<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 * @subpackage user_profile
 */
class user_profile extends WebObject
{
  function on_save($event_args)
    {
      WebApp::execDBCmd("saveUserProfile", $event_args);

      $new_password = $event_args["new_password"];
      if ($new_password != "")
        {
          //check that the old password supplied is correct
          $rs = WebApp::openRS("getPassword");
          $crypted_passwd = $rs->Field("password");
          $old_passwd = $event_args["old_password"];
          if ($crypted_passwd == crypt($old_passwd, $crypted_passwd))
            {
              //encrypt the new password and save it
              srand(time());
              $password = crypt($new_password, rand());
              $query_vars["password"] = $password;
              WebApp::execDBCmd("savePassword", $query_vars);
              WebApp::message("Password changed successfully.");
            }
          else
            {
              $msg = "Old password is wrong!\n"
                ."Password not changed. Please try again .";
              WebApp::message($msg);
            }         
        }
    }

  function onRender()
    {
      $rs = WebApp::openRS("getUserProfile");
      WebApp::addVars($rs->Fields());
    }
}
?>