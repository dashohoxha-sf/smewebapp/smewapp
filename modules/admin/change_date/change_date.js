// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function set_curr_day()
{
  var form = document.change_date;
  session.setVar("simulated_current_day", form.curr_day.value);
}

function set_curr_month()
{
  var form = document.change_date;
  session.setVar("simulated_current_month", form.curr_month.value);
}

function set_curr_year()
{
  var form = document.change_date;
  session.setVar("simulated_current_year", form.curr_year.value);
}

function set_actual_values()
{
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth() + 1;
  var year = date.getFullYear();
  var form = document.change_date;

  session.setVar("simulated_current_day", ''+day);
  session.setVar("simulated_current_month", ''+month);
  session.setVar("simulated_current_year", ''+year);

  form.curr_day.value = day;
  form.curr_month.value = month;
  form.curr_year.value = year;

  refresh();
}

function int2mon(m_id)
{
  var arr_months = new Array(
                             'Jan', 'Feb', 'Mar', 'Apr', 
                             'May', 'Jun', 'Jul', 'Aug', 
                             'Sep', 'Oct', 'Nov', 'Dec' );
  return arr_months[m_id - 1];
}
