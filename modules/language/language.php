<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package main
 */
class language extends WebObject
{
  function init()
    {
      WebApp::addSVar('language', LNG);
      WebApp::addSVar('codeset', CODESET);
    }

  function on_select($event_args)
    {
      //set the selected language
      $lng = $event_args['lng'];
      WebApp::setSVar('language', $lng);

      //set the corresponding codeset
      $rs = WebApp::openRS('get-charset');
      $charset = $rs->Field('charset');
      WebApp::setSVar('codeset', $charset);

      //recreate the user menu
      include_once MENU.'func.create_user_menu.php';
      create_user_menu();
    }

  function onParse()
    {
      $lng = WebApp::getSVar('language');
      $codeset = WebApp::getSVar('codeset');
      global $l10n;
      $l10n->set_lng($lng, $codeset);
    }
}
?>