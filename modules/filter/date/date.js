// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function get_day(txtbox)
{
  var day = txtbox.value;
  day = ""+day;
  day = day.replace(/ +/g, '');
  if (day=="")  day = "1";
  txtbox.value = day;
  return day;
}

function get_year(txtbox)
{
  var year = txtbox.value;
  year = ""+year;
  year = year.replace(/ +/g, '');
  if (year=="")
    {
      var current_date = get_curr_date();
      year = ""+current_date.getFullYear();
    }
  txtbox.value = year;
  return year;
}

function set_from_date(element)
{
  var form = element.form;

  //get selected day1, month1 and year1 and create the object date1
  var day1 = get_day(form.day1);
  var month1 = form.month1.selectedIndex;
  var year1 = get_year(form.year1);
  var date1 = new Date(year1, month1, day1);

  //get selected day2, month2 and year2 and create the object date2
  var day2 = get_day(form.day2);
  var month2 = form.month2.selectedIndex;
  var year2 = get_year(form.year2);
  var date2 = new Date(year2, month2, day2);

  //check that date1 is not greater then date2
  if (date1 <= date2)
    {
      session.setVar("date->day1", day1);
      session.setVar("date->month1", ""+(month1+1));
      session.setVar("date->year1", year1);
    }
  else
    {
      alert(T_("Starting date cannot be greater than ending date."));

      //set From date equal to To date
      form.day1.value = ""+day2;
      form.month1.options[month2].selected = true;
      form.year1.value = ""+year2;
      //change it in the session as well
      session.setVar("date->day1", day2);
      session.setVar("date->month1", ""+(month2+1));
      session.setVar("date->year1", ""+year2);
    }
}

function set_to_date(element)
{
  var form = element.form;

  //get selected day1, month1 and year1 and create the object date1
  var day1 = get_day(form.day1);
  var month1 = form.month1.selectedIndex;
  var year1 = get_year(form.year1);
  var date1 = new Date(year1, month1, day1);

  //get selected day2, month2 and year2 and create the object date2
  var day2 = get_day(form.day2);
  var month2 = form.month2.selectedIndex;
  var year2 = get_year(form.year2);
  var date2 = new Date(year2, month2, day2);

  //check that date2 is not smaller then date1
  if (date2 < date1)
    {
      alert(T_("Ending date cannot be smaller than the starting date."));

      //set To date equal to From date
      form.day2.value = ""+day1;
      form.month2.options[month1].selected = true;
      form.year2.value = ""+year1;
      //change it in the session as well
      session.setVar("date->day2", day1);
      session.setVar("date->month2", ""+(month1+1));
      session.setVar("date->year2", year1);

      return;
    }

  //create curr_date
  var current_date = get_curr_date();
  var curr_day = current_date.getDate() +"";
  var curr_month = current_date.getMonth();
  var curr_year = current_date.getFullYear();
  var curr_date = new Date(curr_year, curr_month, curr_day);

  //check that to_date is not greater then curr_date
  if (date2 <= curr_date)
    {
      session.setVar("date->day2", day2);
      session.setVar("date->month2", ""+(month2+1));
      session.setVar("date->year2", year2);
    }
  else //to_date is greater then curr_date
    {
      var msg = T_("Ending date cannot be greater than the current date.");
      alert(msg);

      //set To date equal to the current date
      form.day2.value = ""+curr_day;
      form.month2.options[curr_month].selected = true;
      form.year2.value = ""+curr_year;
      //set it in the session as well
      session.setVar("date->day2", ""+curr_day);
      session.setVar("date->month2", ""+(curr_month+1));
      session.setVar("date->year2", ""+curr_year);
    }
}
