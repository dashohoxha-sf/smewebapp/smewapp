<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package filter
 * @subpackage date
 */
class date extends WebObject
{
  function init()
    {
      $curr_day = get_curr_date("d");
      $curr_month = get_curr_date("n");
      $curr_year = get_curr_date("Y");
      $this->addSVars( array( "day1" => '01',
                              "month1" => $curr_month,
                              "year1" => $curr_year,
                              "day2" => $curr_day,
                              "month2" => $curr_month,
                              "year2" => $curr_year
                              ) );
      $this->addSVar("filter", "");
    }

  function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }

  function get_filter()
    {
      $day1   = $this->getSVar("day1");
      $month1 = $this->getSVar("month1");
      $year1  = $this->getSVar("year1");
      $day2   = $this->getSVar("day2");
      $month2 = $this->getSVar("month2");
      $year2  = $this->getSVar("year2");

      $filter = "(date_field >= '$year1-$month1-$day1'"
        . " AND date_field <= '$year2-$month2-$day2')";
      return $filter;
    }

  function onRender()
    {
      //get recordset 'monthList'
      $rs = $this->get_month_rs();
      global $webPage;
      $webPage->addRecordset($rs);

      //add the {{vars}} for the selected months
      for ($m=1; $m <= 12; $m++)
        {
          WebApp::addVar("sel1_$m", "");
          WebApp::addVar("sel2_$m", "");
        }
      $month1 = $this->getSVar("month1");
      $month2 = $this->getSVar("month2");
      WebApp::addVar("sel1_".$month1, "selected");
      WebApp::addVar("sel2_".$month2, "selected");
    }

  /** build and return the recordset 'monthList' */
  function get_month_rs()
    {
      $rs = new EditableRS("monthList");
      $rs->Open();
      for ($i=1; $i<=12; $i++)
        {
          $rec = array("m_id"=>$i, "Mon"=>int2mon($i));
          $rs->addRec($rec);
        }
      $rs->MoveFirst();

      return $rs;
    }
}
?>