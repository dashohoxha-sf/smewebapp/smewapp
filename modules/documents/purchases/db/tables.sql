-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS purchases;
CREATE TABLE purchases (
  purchase_id int(10) unsigned NOT NULL auto_increment,
  purchase_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  item varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  source varchar(100) default '',
  notes text,
  PRIMARY KEY  (purchase_id)
) TYPE=MyISAM;

