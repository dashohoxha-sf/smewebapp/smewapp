<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    documents
 * @subpackage purchases
 */
class purchasePrint extends WebObject
{
  function init()
    {
      $this->addSVar("purchase_id", $purchase_id);
    }

  function onParse()
    {
      $path = dirname(__FILE__);
      $latex_tpl = $path.'/latex/purchase.tex';
      global $event;
      $vars = array('purchase_id' => $event->args['purchase_id']);
      get_pdf_file($latex_tpl, $vars);
    }

  function onRender()
    {
      $rs = WebApp::openRS("get_purchase");
      $fields = $rs->Fields();

      //pad fields with spaces
      while (list($fld_name, $fld_value) = each($fields))
        {
          $fld_value = $this->pad($fld_value);
          $padded_fields[$fld_name] = $fld_value;
        }

      WebApp::addVars($padded_fields);
    }

  /** pad a field with spaces, if it is shorter than 5 chars */
  function pad($field)
    {
      $padded_field = $field;
      for ($i=strlen($field); $i < 5; $i++)
        {
          $padded_field = '<span class="h_space"/>'.$padded_field;
        }
      return $padded_field;
    }
}
?>