// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/*
function help(interface_id)
{
  if (interface_id==null)  interface_id = session.getVar("interface");
  var url = '../smewapp-docs/xref.php?/smewebapp_manual/'+interface_id;
  window.open(url, 'help');
}
*/

function help(topic)
{
  if (topic==null)  topic = session.getVar("interface");
  select('help', 'topic='+topic);
}

/** reloads the current page */
function refresh()
{
  GoTo('thisPage');
}

function print_interface(interface_id, args)
{
  if (interface_id==null)  interface_id = session.getVar("interface");
  var event_args = "interface=" + interface_id;
  if (args != null)  event_args += ";" + args;

  document.WebAppForm.target = '_blank'; //open it in a new window
  GoTo("print/print.html?event=print_interface.select("+event_args+")");
  document.WebAppForm.target = '';
}

function print_template(interface_id, args)
{
  if (interface_id==null)  interface_id = session.getVar("interface");
  var event_args = "interface=" + interface_id;
  if (args != null)  event_args += ";" + args;

  document.WebAppForm.target = '_blank'; //open it in a new window
  GoTo("print/print_tpl.html?event=print_template.select("+event_args+")");
  document.WebAppForm.target = '';
}
