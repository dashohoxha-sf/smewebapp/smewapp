<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package main
 */
class main extends WebObject
{
  /**
   * Returns true when the current user has no rights to access 
   * the given interface.
   */ 
  function has_no_access($interface)
    {
      $u_id = WebApp::getSVar('u_id');
      if ($u_id==0)  return false; //superuser has no restrictions

      if ($interface=='user_profile' or $interface='help')  return false;

      $access_rights = WebApp::getSVar('access_rights');
      $arr_access_rights = explode(',', $access_rights);
      $has_access = in_array($interface, $arr_access_rights);

      return !$has_access;
    }

  function on_select($event_args)
    {
      $interface = $event_args['interface'];
      if ($this->has_no_access($interface))
        {
          $msg = T_("You have no access to the interface 'v_interface'.");
          $msg = str_replace('v_interface', $interface, $msg);
          WebApp::message($msg);
          return;
        }

      //user can access this interface

      WebApp::setSVar('interface', $interface);

      switch ($interface)
        {
        default:
          $msg = T_("Unknown interface 'v_interface'.");
          $msg = str_replace('v_interface', $interface, $msg);
          WebApp::message($msg);
          return;
          break;

          //documents
        case 'documents/purchases/list':
        case 'documents/purchases/edit':
        case 'documents/purchases/new':
          $interface_title = T_("documents");
          $module = 'documents/documents.html';
          break;

          //inventory
        case 'inventory/warehouse':
          $interface_title = T_("inventory");
          $module = 'inventory/inventory.html';
          break;

          //reports
        case 'reports/purchases':
          $interface_title = T_("reports");
          $module = 'reports/reports.html';
          break;

          //tables
        case 'tables/items':
          $interface_title = T_("Tables");
          $module = 'tables/tables.html';
          break;
        case 'tables/simple_tables/group1':
        case 'tables/simple_tables/group2':
        case 'tables/simple_tables/group3':
          $interface_title = T_("Tables");
          $module = 'tables/tables.html';
          break;

          //admin
        case 'superuser':
        case 'user_profile':
        case 'change_date':
        case 'admin/users':
        case 'admin/logs':
        case 'backup':
          $interface_title = T_("Admin");
          $module  = 'admin/admin.html';
          break;

          //help
        case 'help':
          $interface_title = T_("Help");
          $module  = 'help/help.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);    
      WebApp::setSVar('module', $module);    
    }
}
?>