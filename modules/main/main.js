// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function select(interface_id, args)
{
  var event_args = "interface="+interface_id;
  if (args != null)  event_args += ";" + args;
  SendEvent("main", "select", event_args);
}

/**
 * Returns the current date, but if the application is
 * in testing mode, then it returns the simulated current date.
 */
function get_curr_date()
{
  var curr_date;

  if (session.isset("simulated_current_year"))
    {
      var day = session.getVar("simulated_current_day");
      var month = session.getVar("simulated_current_month");
      var year = session.getVar("simulated_current_year");
      curr_date = new Date(year, month-1, day);
    }
  else
    {
      curr_date = new Date();
    }

  return curr_date;
}
