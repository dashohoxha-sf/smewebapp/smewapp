<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004, 2005, 2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package inventory
 */
class inventory extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'inventory/warehouse/itemList.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_inventory($event->args);
        }
    }

  function select_inventory($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //warehouse
        case 'inventory/warehouse':
          $interface_title = T_("Warehouse");
          $module = 'warehouse/wItemList.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>