
DROP TABLE IF EXISTS warehouse;
CREATE TABLE warehouse (
  wItem_id int(11) NOT NULL auto_increment,
  name varchar(20) default NULL,
  unit varchar(10) default NULL,
  quantity int(11) default NULL,
  price int(11) default NULL,
  value int(11) default NULL,
  PRIMARY KEY  (wItem_id)
) TYPE=MyISAM COMMENT='The table that contains wItems.';

