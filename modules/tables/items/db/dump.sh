#!/bin/bash
### dump the DB tables of the module

### go to this directory
cd $(dirname $0)

### get the variables
. vars

### dump the database $dbname into the file dbtables.sql
mysqldump --add-drop-table --allow-keyword --no-data \
          --host=$host --user=$user --password=$passwd \
          $database items > dbtables.sql

